import struct

u64 = lambda x: struct.unpack('<Q', x)[0]

begin = 0x7ffffffde000
end = 0x7ffffffff000

search = [(0x555555559000, 0x555555563000), (0x7ffff7ffc000, 0x7ffff7fff000)]

def read_mem(ptr, size):
    gdbi = gdb.inferiors()[0]
    m = gdbi.read_memory(ptr, size)
    return m.tobytes()

def main():
    p = begin
    while p < end:
        v = u64(read_mem(p, 8))
        for r in search:
            if r[0] <= v <= r[1]:
                print("0x{:x} --> 0x{:x}".format(p, v))
                break
        p += 8

main()
