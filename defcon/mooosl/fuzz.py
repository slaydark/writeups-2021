import json
from ptrlib import *
from z3 import *

"""
typedef struct {
  char *key;    // +00h
  char *val;    // +08h
  int key_size; // +10h: size of key actually stored
  int val_size; // +18h: size of value actually stored
  int hash;     // +20h: hash of key
  struct Pair* next; // +28h: next
} Pair;
"""

def store(key_size, key, value_size, value):
    sock.sendlineafter("option: ", "1")
    sock.sendlineafter(": ", str(key_size))
    sock.sendafter(": ", key)
    sock.sendlineafter(": ", str(value_size))
    sock.sendafter(": ", value)
def query(key_size, key):
    sock.sendlineafter("option: ", "2")
    sock.sendlineafter(": ", str(key_size))
    sock.sendafter(": ", key)
    l = sock.recvline()
    if l == b'err':
        return None
    r = l.split(b':')
    return int(r[0], 16), bytes.fromhex(r[1].decode())
def delete(key_size, key):
    sock.sendlineafter("option: ", "3")
    sock.sendlineafter(": ", str(key_size))
    sock.sendafter(": ", key)

def calc_hash(key):
    if isinstance(key, str):
        key = key.encode()
    h = 0x7e5
    for c in key:
        h = (h * 0x13377331 + c) & 0xffffffff
    return h

def collide(hashval, base, length=4, prefix=True):
    if isinstance(base, str):
        base = base.encode()
    h = 0x7e5
    append = [BitVec(f'c_{i}', 8) for i in range(length)]
    if not prefix:
        for c in append:
            h = (h * 0x13377331 + ZeroExt(24, c)) & 0xffffffff
    for c in base:
        h = (h * 0x13377331 + c) & 0xffffffff
    if prefix:
        for c in append:
            h = (h * 0x13377331 + ZeroExt(24, c)) & 0xffffffff
    s = Solver()
    s.add(h == hashval)
    r = s.check()
    if r == sat:
        m = s.model()
        return bytes([m[c].as_long() for c in append])
    else:
        print("[-] Try with longer length")
        exit(1)

sock = process("./mooosl")

maxlen = 5
for i in range(10000):
    choice = random_int(0, 2)
    if choice == 0:
        key_size = random_int(0, maxlen)
        key = random_bytes(0, key_size, list(set(range(0x100)) - set([0x0a])))
        if len(key) < key_size:
            key += b'\n'
        val_size = random_int(0, maxlen)
        val = random_bytes(0, val_size, list(set(range(0x100)) - set([0x0a])))
        if len(val) < val_size:
            val += b'\n'
        print(json.dumps(
            {"func": "store",
             "args": [key_size, bytes2str(key), val_size, bytes2str(val)]}
        ))
        store(key_size, key, val_size, val)
    elif choice == 1:
        continue
        size = random_int(0, maxlen)
        key = random_bytes(0, size, list(set(range(0x100)) - set([0x0a])))
        if len(key) < size:
            key += b'\n'
        print(json.dumps(
            {"func": "query",
             "args": [size, bytes2str(key)]}
        ))
        query(size, key)
    else:
        size = random_int(0, maxlen)
        key = random_bytes(0, size, list(set(range(0x100)) - set([0x0a])))
        if len(key) < size:
            key += b'\n'
        print(json.dumps(
            {"func": "delete",
             "args": [size, bytes2str(key)]}
        ))
        delete(size, key)

sock.close()
