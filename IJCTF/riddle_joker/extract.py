import zlib
from PIL import Image

with open("secret.pdf", "rb") as f:
    buf = f.read()

pos = 0
seen = set()
images = []
while b'>>\nstream\n' in buf[pos:]:
    pos = buf.index(b'>>\nstream\n', pos) + 10
    end = buf.index(b'\nendstream', pos)
    try:
        ponta = zlib.decompress(buf[pos:end])
        if hash(ponta) in seen:
            continue
        if len(ponta) == 4900:
            images.append(ponta)
            seen.add(hash(ponta))
    except:
        pass

for j, image in enumerate(images):
    img = Image.new("RGB", (70, 70), (255, 255, 255))
    i = 0
    for y in range(70):
        for x in range(70):
            img.putpixel((x, y), (image[i], image[i], image[i]))
            i += 1
    img.save(f"img-{j}.png")
