import zlib
import re
from PIL import Image

with open("secret.pdf", "rb") as f:
    buf = f.read()

pos = 0
seen = set()
images = []
while b'/XObject << /Im' in buf[pos:]:
    pos = buf.index(b'/XObject << /Im', pos)
    pos = buf.rindex(b'endobj\n', 0, pos) + 7
    r = re.findall(b"(\d+) (\d+) obj", buf[pos:pos+0x20])
    target = f'/Kids [ {int(r[0][0])} {int(r[0][1])} R ]'
    p = buf.index(target.encode())
    p = buf.index(b"/Coordinate", p)
    e = buf.index(b"\n", p)
    r = re.findall(b"\((\d+),(\d+)\)", buf[p:e].replace(b'\x00', b''))
    x, y = int(r[0][0]), int(r[0][1])
    while b'>>\nstream\n' in buf[pos:]:
        pos = buf.index(b'>>\nstream\n', pos) + 10
        end = buf.index(b'\nendstream', pos)
        try:
            ponta = zlib.decompress(buf[pos:end])
            if hash(ponta) in seen:
                break
            if len(ponta) == 4900:
                images.append((x, y, ponta))
                seen.add(hash(ponta))
                break
        except:
            pass

ylen = 7
xlen = 7
img = Image.new("RGB", (70*xlen,70*ylen), (255,255,255))

for (x, y, image) in images:
    cnt = 0
    for i in range(70):
        for j in range(70):
            img.putpixel((x+j, y+i), (image[cnt], image[cnt], image[cnt]))
            cnt += 1

img.save("output.png")
    
