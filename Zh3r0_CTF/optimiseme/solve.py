import sympy as sym

data = bytes.fromhex('666c6167203a20757e164568ead24c52bc0520605dff4acc18205b761b891db53489d1f2de141b91ab5347000a00cccccccc')
data = data[7:]

def f(n):
    x = sym.symbols('x', nonnegative=True, integer=True)
    Fib = 1 / sym.sqrt(5) * (((1+sym.sqrt(5))/2)**(n-1) - ((1-sym.sqrt(5))/2)**(n-1))
    result = Fib.subs(x, n)
    result = sym.simplify(result)
    return int(result)

def g(v):
    h = 0
    while v != 0:
        h ^= (v % 10)
        v //= 10
    return h

def decode_char(c, i):
    v = f(i)
    return (c ^ v ^ g(v)) & 0xff

for i in range(len(data)):
    print(chr(decode_char(data[i], 8+i)), end='')
