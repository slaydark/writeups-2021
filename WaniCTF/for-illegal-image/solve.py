from scapy.all import *

data = b''
def analyse(pkt):
    global data
    if pkt[ICMP].type == 8:
        data += pkt[Raw].load
    return

sniff(offline='illegal_image.pcap', filter='icmp', store=0, prn=analyse)

with open("image.jpeg", "wb") as f:
    f.write(data)
