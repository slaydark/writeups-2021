def encrypt(plaintext: str, a: int, b: int) -> str:
    ciphertext = ""
    for x in plaintext:
        if "A" <= x <= "Z":
            x = ord(x) - ord("A")
            x = (a * x + b) % 26
            x = chr(x + ord("A"))
        ciphertext += x

    return ciphertext

with open("output.txt", "r") as f:
    cipher = f.read()
plaintext_space = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_{}"

flag = "FLAG"
for a in range(26):
    for b in range(26):
        if encrypt(flag, a, b) == cipher[:len(flag)]:
            break
    else:
        continue
    break

print(a, b)
for i in range(len(flag), len(cipher)):
    for c in plaintext_space:
        if encrypt(flag+c, a, b) == cipher[:len(flag)+1]:
            flag += c
            break

print(flag)
