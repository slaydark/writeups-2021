bits = 0
with open("binary.csv", "r") as f:
    for line in f:
        bits <<= 1
        bits |= int(line)

print(int.to_bytes(bits, 512, "big"))
