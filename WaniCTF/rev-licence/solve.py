import angr
import claripy
from logging import getLogger, WARN

getLogger("angr").setLevel(WARN + 1)
getLogger("claripy").setLevel(WARN + 1)

data = claripy.BVS('data', 0x1000)
simfile = angr.SimFile('key.dat', content=data)

p = angr.Project("./licence", load_options={"auto_load_libs": False})
state = p.factory.entry_state(
    args=[p.filename, simfile.name],
    fs={simfile.name: simfile}
)
simgr = p.factory.simulation_manager(state)

simgr.explore(find=0x400000 + 0x5e57)
#simgr.explore(find=0x400000 + 0x5d9e)
data = simgr.found[0].fs.get(simfile.name).concretize()

with open("result.key", "wb") as f:
    f.write(data)
