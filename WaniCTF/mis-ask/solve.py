bits = 0
with open("ask.csv", "r") as f:
    w = 0
    for line in f:
        if w % 31 == 0:
            bits <<= 1
            bits |= int(line)
        w += 1

print(bin(bits))
print(int.to_bytes(bits, 512, 'big'))
