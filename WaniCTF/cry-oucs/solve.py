from ptrlib import *
from server import OkamotoUchiyamaCryptoSystem
from Crypto.Util.number import bytes_to_long, long_to_bytes

def get_eflag():
    sock.sendlineafter("> ", "1")
    return int(sock.recvlineafter("ciphertext = "), 16)

def get_pubkey():
    sock.sendlineafter("> ", "4")
    n = int(sock.recvlineafter("n = "), 16)
    g = int(sock.recvlineafter("g = "), 16)
    h = int(sock.recvlineafter("h = "), 16)
    return n, g, h

def enc(m):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", str(m))

def dec(c):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", str(c))

#sock = Process(["python", "server.py"])
sock = Socket("nc oucs.cry.wanictf.org 50010")
oucs = OkamotoUchiyamaCryptoSystem(1024)
oucs.p = -1

cflag = get_eflag()
logger.info("cflag = " + hex(cflag))

oucs.n, oucs.g, oucs.h = get_pubkey()
logger.info("n = " + hex(oucs.n))
logger.info("g = " + hex(oucs.g))
logger.info("h = " + hex(oucs.h))

c1 = bytes_to_long(oucs.encrypt(b"a"))
c = (c1 * cflag) % oucs.n
dec(c)
m = int(sock.recvlineafter("= "), 16)
print(long_to_bytes(m))

sock.interactive()
