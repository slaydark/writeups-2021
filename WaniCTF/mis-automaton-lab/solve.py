from ptrlib import *

pattern = {
    (1,1,1): 0,
    (1,1,0): 0,
    (1,0,1): 0,
    (1,0,0): 1,
    (0,1,1): 1,
    (0,1,0): 1,
    (0,0,1): 1,
    (0,0,0): 0,
}

def calc(init, gen):
    state = list(init)
    i = 0
    h = set()
    start = None
    while i < gen:
        hh = hash(tuple(state))
        if start is None:
            if hh in h:
                start = i
                ponta = hh
                foota = list(state)
            else:
                h.add(hh)
        elif hh == ponta:
            cycle = i - start
            print(cycle)
            return calc(state, (gen - start) % cycle)
            
        cur = list(state)
        for j in range(len(state)):
            state[j] = pattern[(cur[j-1], cur[j], cur[(j+1)%len(cur)])]
        if state == init:
            print("ponta")
            gen %= i
        i += 1
    return ''.join(map(str, state))

sock = Socket("nc automaton.mis.wanictf.org 50020")

sock.sendlineafter("continue)", "")

for i in range(3):
    init = list(map(int, sock.recvlineafter("init = ").decode()))
    gen = int(sock.recvlineafter("gen = "))
    print(init, gen)
    sock.sendlineafter("> ", calc(init, gen))

sock.interactive()
