from z3 import *

dest = [
    0x3E, 0x49, 0x26, 0x52, 0x45, 0x22, 0x42, 0x10, 0x66, 0x0B, 
    0x6C, 0x06, 0x0D, 0x50, 0x0F, 0x4C, 0x25, 0x4C, 0x3F, 0x12, 
    0x56, 0x03, 0x20, 0x5A, 0x14, 0x61, 0x4A, 0x3F, 0x5D, 0x51, 
    0x12, 0x5C, 0x18, 0x05, 0x43, 0x39, 0x4F, 0x32, 0x0A
]

s = Solver()

flag = [BitVec(f'flag_{i}', 8) for i in range(39)]
for c in flag:
    s.add(0x20 <= c, c < 0x7f)
#for c in dest:
#    s.add(Or(And(0x20 <= c, c < 0x7f),
#             c == 0, c == 0x0a, c == 0x0d))
for i, j in enumerate("justCTF{"):
    s.add(flag[i] == ord(j))
s.add(flag[-1] == ord('}'))

for i in range(39):
    for j in range(i, 39):
        dest[j] ^= flag[i]

t = 0
for c in flag:
    t += ZeroExt(8, c)
s.add(t == 0x0d9f)
t = 0
for c in dest:
    t += ZeroExt(8, c)
s.add(t == 0x0fd9)

while True:
    r = s.check()
    if r == sat:
        m = s.model()
        output = ''
        for c in flag:
            output += chr(m[c].as_long())
        print(output)
        cs = []
        for c in flag:
            cs.append(c == m[c].as_long())
        s.add(Not(And(cs)))
    else:
        print(r)
        break
