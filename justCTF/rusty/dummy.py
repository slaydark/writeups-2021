from z3 import *

x = b''
x += int.to_bytes(0x13C012000FB00FB011B013B01440145, 16, 'little')
x += int.to_bytes(0x1190140012C0141013B014701420151, 16, 'little')
x += int.to_bytes(0x138013201350143015D014701160119, 16, 'little')
x += int.to_bytes(0x13E014201430149014A013A01300136, 16, 'little')
x += int.to_bytes(0x0D600D100D200E600D900F200FA0134, 16, 'little')
x += int.to_bytes(0x0BF00630063008900A900D400D300D7, 16, 'little')
x += int.to_bytes(0x14A0108, 4, 'little')

s = Solver()
flag = [BitVec(f'flag_{i}', 8) for i in range(0x37)]

s.add(flag[0] == ord('j'))
s.add(flag[1] == ord('c'))
s.add(flag[2] == ord('t'))
s.add(flag[3] == ord('f'))
s.add(flag[4] == ord('{'))
s.add(flag[0x36] == ord('}'))

for i in range(7, 0x39):
    a = ZeroExt(8, flag[(i-2) % len(flag)])
    b = ZeroExt(8, flag[(i-1) % len(flag)])
    c = ZeroExt(8, flag[i % len(flag)])
    t = a + b + c
    s.add(t == int.from_bytes(x[i*2-0x0e:i*2-0x0e+2], 'little'))

r = s.check()
if r == sat:
    m = s.model()
    output = ''
    for c in flag:
        output += chr(m[c].as_long())
    print(output)
else:
    print(r)
