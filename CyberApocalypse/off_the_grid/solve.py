import re

oled = [[0 for j in range(132)] for i in range(64)]
opeseq = []
with open("spi.txt", "r") as f:
    for line in f:
        r = re.findall(",0,0x([0-9A-F]+)", line)
        if r:
            opeseq.append(int(r[0], 16))

opeseq = opeseq[opeseq.index(0xaf)+1:] # skip initialize phase

x, y = 0, 0
# decode
for rnd in range(6):
    for i in range(8):
        for ope in opeseq[:3]:
            if ope & 0xf0 == 0xb0:
                y = ope & 0xf # Set row
            elif ope & 0xf0 == 0x00:
                x = (x & 0xf0) | (ope & 0x0f) # Set column (lower)
            elif ope & 0xf0 == 0x10:
                x = (x & 0x0f) | ((ope & 0x0f) << 4) # Set column (upper)
            else:
                print("[-] :thinking_face:")
                exit(1)
        opeseq = opeseq[3:]
        for i in range(128):
            oled[y][x+i] = opeseq[i]
        opeseq = opeseq[128:]

    # show
    output = ""
    for y in range(8):
        for j in range(8):
            for x in range(132):
                b = (oled[y][x] >> j) & 1
                output += "#" if b else "."
            output += "\n"
    print(output)

    print("\n\n" + "=" * 128 + "\n\n")

# CHTB{013d_h4ck1n9_f7w!2^25#}
