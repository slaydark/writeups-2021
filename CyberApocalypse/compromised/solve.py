import re
output = {}
with open("output.txt", "r") as f:
    for line in f:
        r = re.findall("write to 0x([0-9A-F]+) ack data: 0x([0-9A-F]+)", line)
        if r:
            k = int(r[0][0], 16)
            c = int(r[0][1], 16)
            if k not in output:
                output[k] = b''
            output[k] += bytes([c])

print(output)
