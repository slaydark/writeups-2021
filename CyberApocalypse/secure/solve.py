with open("output.txt", "r") as f:
    buf = f.read()

output = ""
for block in buf.split("/"):
    for data in block.split():
        c = int(data, 16)
        if c == 0 or c == 0xff:
            continue
        output += chr(c)

print(output)
