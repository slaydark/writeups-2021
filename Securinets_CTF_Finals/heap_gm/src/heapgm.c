#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>

#define MAX_SIZE 0x4000
#define MAX_CHUNKS 0x11


void* tcache;

void __attribute__((constructor)) init(){
	setbuf(stdin, NULL);
	setbuf(stdout, NULL);
	alarm(60*4);
	tcache = (void*)malloc(1) - 0x2a0 + 0x10;
}

char *chunks[MAX_CHUNKS] = {0};
int32_t sizes[MAX_CHUNKS] = {0};

extern void* __free_hook;
extern void* __malloc_hook;
extern void* environ;

uint32_t take_index(){
	uint32_t i;
	printf("Index > ");
	scanf("%d", &i);

	if(i >= MAX_CHUNKS){
		puts("OOBounds are not allowed here!");
		exit(0);
	}
	return i;
}

void clear_tcache(){
	memset(tcache, 0x0, 0x280);
}

void clear_addresses(){
	// This function is defined to make sure you're not using
	// any one of these in your exploit.
	__malloc_hook = NULL;
	__free_hook   = NULL;
	environ       = NULL;
}

void allocate_mem(){

	uint32_t size;
	uint32_t i;
	char* chunk;
	int ret;

	i = take_index();

	printf("Size > ");
	scanf("%d", &size);
	if(size > MAX_SIZE) {
		puts("Too big to allocate.");
		return;
	}
	chunk = (char*)malloc(size);
	
	printf("data > ");
	ret = read(0, chunk, size);
	
	if(ret % 4 != 0){
		puts("Sorry but I don't allow partial overwrites :/");
		exit(0);
	}

	if(ret < 0){
		puts("Something went wrong!");
		exit(0);
	}

	chunks[i] = chunk;
	sizes[i]  = size;

	chunk[ret]= 0x0;
	clear_addresses();
}

void free_mem(){
	uint32_t i;
	i = take_index();
	
	if(chunks[i]){
		memset(&__free_hook, 0, 8);
		free(chunks[i]);
		chunks[i] = NULL;
		sizes[i] = 0x0;
		clear_tcache();
	} else {
		puts("Nothing to free!");
		return;
	}
}

void show_mem(){
	uint32_t i;
	i = take_index();
	if(chunks[i] != NULL)
		puts(chunks[i]);
	else
		puts("Nothing in here");
}

void edit_mem(){
	uint32_t i;
	int ret;

	i = take_index();
	if(chunks[i]){
		printf("Data > ");
		ret = read(0, chunks[i], sizes[i]);
		if(ret % 4 != 0){
			puts("Sorry but I don't allow partial overwrites :/");
			exit(0);
		}
		chunks[i][ret] = 0x0;
	} else {
		puts("Nothing to edit!");
	}
	clear_addresses();
}

void menu(){
	puts("[0] Allocate");
	puts("[1] Free");
	puts("[2] Show");
	puts("[3] edit");
	puts("[4] Bye");
	printf("> ");
}

int main(){
	int choice;
	char name[0x10];
	memset(environ, 0, 8);
	puts("Enter your name : ");
	name[read(0, name, sizeof(name)-1)-1] = 0x0;
	while(1){
		menu();
		scanf("%d", &choice);
		switch(choice){
			case 0:
				allocate_mem();
				break;
			case 1:
				free_mem();
				break;
			case 2:
				show_mem();
				break;
			case 3:
				edit_mem();
				break;
			case 4:
				printf("Goodbye, %s!\n", name);
				exit(0);
				break;
			default:
				puts("Wrong choice");
		}
	}
}
