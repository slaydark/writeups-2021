from ptrlib import *

with open("encrypted.txt", "r") as f:
    r = f.read().split()

flag = b""
for b in r:
    flag += p64(int(b, 16) ^ 0xd4c70f8a67d5456d, "big")

print(flag)
