import re

s = """
mov     [rbp+var_40], 63h ; 'c'
mov     [rbp+var_3E], 65h ; 'e'
mov     [rbp+var_3C], 64h ; 'd'
mov     [rbp+var_3A], 67h ; 'g'
mov     [rbp+var_38], 79h ; 'y'
mov     [rbp+var_36], 6Ch ; 'l'
mov     [rbp+var_34], 82h
mov     [rbp+var_32], 6Eh ; 'n'
mov     [rbp+var_30], 39h ; '9'
mov     [rbp+var_2E], 7Ch ; '|'
mov     [rbp+var_2C], 7Fh
mov     [rbp+var_2A], 7Eh ; '~'
mov     [rbp+var_28], 41h ; 'A'
mov     [rbp+var_26], 5Ch ; '\'
mov     [rbp+var_24], 6Eh ; 'n'
mov     [rbp+var_22], 79h ; 'y'
mov     [rbp+var_20], 46h ; 'F'
mov     [rbp+var_1E], 71h ; 'q'
mov     [rbp+var_1C], 76h ; 'v'
mov     [rbp+var_1A], 44h ; 'D'
mov     [rbp+var_18], 84h
mov     [rbp+var_16], 65h ; 'e'
mov     [rbp+var_14], 47h ; 'G'
mov     [rbp+var_12], 84h
mov     [rbp+var_10], 96h
"""

encoded = {}
r = re.findall("\[rbp\+var_([0-9A-F]+)\], ([0-9A-F]+)h", s)
for b in r:
    encoded[(0x40 - int(b[0], 16)) // 2] = int(b[1], 16)

flag = ""
for i in range(len(encoded)):
    flag += chr(encoded[i] - i - 1)

print(flag)
