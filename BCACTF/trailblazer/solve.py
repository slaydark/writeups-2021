from ptrlib import *

with open("trailblazer", "rb") as f:
    prog = f.read()

flag = b''

for i in range(8, 0x40, 8):
    code = nasm(f"""
    mov rdx, [rbx+{i}]
    xor [rax+{i}], rdx
    """, bits=64)
    key = xor(prog[0x1262+i-8:0x1262+i], code)
    print(key)

with open("dump", "wb") as f:
    f.write(prog)

