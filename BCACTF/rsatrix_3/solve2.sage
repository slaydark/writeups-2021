from ptrlib import *

def mat_power(base, exp, name):
    sock.sendlineafter(">>> ", "X")
    sock.sendlineafter(">>> ", base)
    sock.sendlineafter(">>> ", str(exp))
    sock.sendlineafter(">>> ", "Y")
    sock.sendlineafter(">>> ", name)

def mat_mul(A, B, name):
    sock.sendlineafter(">>> ", "M")
    sock.sendlineafter(">>> ", A)
    sock.sendlineafter(">>> ", B)
    sock.sendlineafter(">>> ", "Y")
    sock.sendlineafter(">>> ", name)

def get_trace(name):
    sock.sendlineafter(">>> ", "P")
    return int(sock.recvlineafter(name + ": "))

n = 12250029783200708035442688430907155767407534107589849686856901602023044745588908817287475893837114530200770756874643028769505799000457410384361237849623138499604018042429324632369604169982302200676229
e = 3
N = 62
d = 8166686522133805356961792287271437178271689405059899791237934401348696497059272544858317262558076346381188965364240700913300230902627376039004235148937938590077100058175744322454350852499041154941427

#sock = Process(["sudo", "sage", "rt4.sage"])
sock = Socket("nc crypto.bcactf.com 49157")

print("power")
mat_power('G', -e, 'H')
print("mul")
mat_mul('E', 'H', 'X')
print("trace")
trX = get_trace('X')
c = trX * inverse_mod(N, n)
p = power_mod(c, d, n)
print(int.to_bytes(int(p), 256, 'big'))

sock.interactive()
