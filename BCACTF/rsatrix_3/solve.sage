from ptrlib import *

def mat_power(base, exp, name):
    sock.sendlineafter(">>> ", "X")
    sock.sendlineafter(">>> ", base)
    sock.sendlineafter(">>> ", str(exp))
    sock.sendlineafter(">>> ", "Y")
    sock.sendlineafter(">>> ", name)

def mat_mul(A, B, name):
    sock.sendlineafter(">>> ", "M")
    sock.sendlineafter(">>> ", A)
    sock.sendlineafter(">>> ", B)
    sock.sendlineafter(">>> ", "Y")
    sock.sendlineafter(">>> ", name)

def get_trace(name):
    sock.sendlineafter(">>> ", "P")
    return int(sock.recvlineafter(name + ": "))

p = 2118785735523620955301512231868734231925640292462405499978976981762557161416662496081983014179663
q = 1243737700428927574598968208586995066861594665591025213691894901887737529628559457923362470874703
n = p * q
e = 3
N = 31
phi = (p-1) * (q-1)
d = inverse_mod(e, phi)

#sock = Process(["sudo", "sage", "rt3.sage"])
#sock = Socket("nc crypto.bcactf.com 49157")
sock = Socket("nc crypto.bcactf.com 49156")

print("power")
mat_power('G', -e, 'H')
print("mul")
mat_mul('E', 'H', 'X')
print("trace")
trX = get_trace('X')
c = trX * inverse_mod(N, n)
p = power_mod(c, d, n)
print(int.to_bytes(int(p), 64, 'big'))

sock.close()
