X = [
    [...] # pre-defined matrix
]

# Convert flag into matrix
for i in range(5):
    for j in range(5):
        A[5*j + i] = flag[i*5+j]

# B = A^t
for i in range(5):
    for j in range(5):
        B[5*i + j] = A[5*j + i]

# Update A
for i in range(5):
    for j in range(5):
        A[5*i + j] = B[5*i + j] + (i+1)*(j-2)

# ???
_gfortran_cshift0_4(B, ...)

for i in range(5):
    for j in range(5):
        A[5*i + j] = B[5*i + j]

x = 1
for i in range(5):
    for j in range(i):
        C[5*j + i] = x
        C[5*i + j] = x
        x += 1

# D = C * A???
_gfortran_matmul_i4(...)

assert D == X
