from ptrlib import *
import time

for rnd in range(1000):
    sock = Socket("localhost", 9999)
    sock.recvline()

    n = random_int(1, 100)
    sock.sendline(str(n))

    print(n)
    for i in range(n):
        sock.recvline()
        s = random_str(0, 128, " ")
        print(s)
        sock.sendline(s)
        for i in range(16):
            sock.recvline()

    time.sleep(0.1)
    sock.close()
