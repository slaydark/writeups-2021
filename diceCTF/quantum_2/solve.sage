import re
from math import pi

def parse(idx, th=0):
    start = False
    cnt = 0
    skip = 0
    l = [0.0 for i in range(64)]
    with open(f"circuit_parts/circuit_z_{idx}.qasm", "r") as f:
        for line in f:
            if line.startswith("mcphase"):
                start = True
                r = re.findall("mcphase\((.+)\) .+,q\[(\d+)\];", line)
                l[int(r[0][1]) - 192] = eval(r[0][0])
                cnt += 1
            else:
                if start and skip == th:
                    break
                elif start:
                    start = False
                    l = [0.0 for i in range(64)]
                    skip += 1
                    cnt = 0
    #assert len(lack) + cnt >= 64
    # a*2*pi*i / 2^k = c
    t = 0
    r = 0
    for k in range(64):
        a = l[k]*2**k / (2*pi)
        b = (a-t) / 2**(k-1)
        r |= int(round(b)) << k
        t = a
    print(bin(r))
    return r

a = parse(0, 2)
b = parse(1, 2)
c = parse(2, 2)

k1N = a^2 - b
k2N = a^4 - c
N = gcd(k1N, k2N)

p, q = factor(N)
p = p[0]
q = q[0]

e = 65537
ciphertext = [0 for i in range(18)]
ciphertext[0] = 630739985092765888
ciphertext[1] = 451537334074008087
ciphertext[2] = 812262022692002523
ciphertext[3] = 406863754676316755
ciphertext[4] = 272957246833162198
ciphertext[5] = 837206587612398866
ciphertext[6] = 878206764376026653
ciphertext[7] = 74850042067302209
ciphertext[8] = 59941172768887381
ciphertext[9] = 692983716161578660
ciphertext[10] = 977024416743871440
ciphertext[11] = 683876226070993692
ciphertext[12] = 701015663373142030
ciphertext[13] = 603050347939059586
ciphertext[14] = 260768514508744742
ciphertext[15] = 454028599464670705
ciphertext[16] = 494565845408874731
ciphertext[17] = 425724750896288294

d = inverse_mod(e, (p-1)*(q-1))

flag = b''
for c in ciphertext:
    m = power_mod(c, d, N)
    flag += int.to_bytes(int(m), 8, 'big')

print(flag.replace(b'\x00', b''))
