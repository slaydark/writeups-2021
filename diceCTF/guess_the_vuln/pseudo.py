cur = 0

assert len(header[i]) < 0x18

i = 0
while i < len(header):
    c = header[i]

    if c == ord('h'):
        cur -= 1
    elif c == ord('l'):
        cur += 1
    elif c == ord('k'):
        field[cur] += 1
    elif c == ord('j'):
        field[cur] -= 1
    elif c == ord('{'):
        i = jmp_table[i]
    elif c == ord('}'):
        if field[cur] != 0:
            i = jmp_table[i]
            sleep_a_bit()
    else:
        pos = c - 0x30
        if pos < len(flag):
            field[cur] = flag[pos]

    i += 1
