let s = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~ ';

let output = '{\n'
for(let i = 0; i < s.length; i++) {
    output += '"' + s[i] + '": "' + generate(s[i]) + '",\n'
}
output += '}'

console.log(output);
