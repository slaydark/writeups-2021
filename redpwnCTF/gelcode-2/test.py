from pwn import *
from ptrlib import str2bytes, brute_force_attack, brute_force_pattern

for length in range(1, 10):
    for pattern in brute_force_attack(length, table_len=6):
        code = str2bytes(
            brute_force_pattern(pattern, '\x00\x01\x02\x03\x04\x05')
        )
        r = disasm(code, arch='amd64')
        if '...' not in r and '.byte' not in r:
            print(r)
