cost_a = 2
cost_b = 8
n = 256

MAX = 2 ** 32

dp = [[MAX] * n for _ in range(n)]
operation = [[""] * n for _ in range(n)]

for i in range(n):
  for d in range(0, 6):
    dp[i][(i + d) % n] = cost_a
    operation[i][(i + d) % n] = f"A{d}"
    dp[i][(i + i + d) % n] = cost_b
    operation[i][(i + i + d) % n] = f"B{d}"

for i in range(n):
  for j in range(n):
    for k in range(n):
      if dp[j][i] + dp[i][k] < dp[j][k]:
        dp[j][k] = dp[j][i] + dp[i][k]
        operation[j][k] = operation[j][i] + operation[i][k]

import pickle
with open("dp", "wb") as f:
    pickle.dump(dp, f)
with open("operation", "wb") as f:
    pickle.dump(operation, f)
