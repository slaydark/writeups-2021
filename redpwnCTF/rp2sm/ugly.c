#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#define MKTYPE(n) typedef int##n##_t i##n; typedef uint##n##_t u##n;
MKTYPE(8) MKTYPE(16) MKTYPE(32) MKTYPE(64)

#define BYTE(n)       ((n) & 0xff)
#define WORD(n)       BYTE(n), BYTE((n)>>8)
#define DWORD(n)      WORD(n), WORD((n)>>16)

#define PUSH32(n)     8, DWORD(n)
#define PUSH16(n)     9, WORD(n)
#define PUSH8(n)      10, BYTE(n)
#define DUP           2
#define POP           1
#define CALL(n)       16, WORD(n)
#define JMP_END       17
#define MAKE_LABEL    18
#define JMP(n)        19, WORD(n)
// pop rax; test eax, eax; jnz jmp_vec2[index]
#define JNZ(n)        20, WORD(n)
#define AND           80
#define OR            81
#define XOR           82
#define ADD           83
#define SUB           84
#define SHL           92
#define SHR           93
#define SAR           94
#define IS_EQ_ZERO    64
#define IS_NE_ZERO    65
#define CMP_EQ        66
#define CMP_NE        67
#define CMP_LT        68
#define CMP_GT        69
#define CMP_LE        70
#define CMP_GE        71
#define CMP_ULT       72
#define CMP_UGT       73
#define CMP_ULE       74
#define CMP_UGE       75
#define LDLOC(n)      32, BYTE(n)
#define LDARG(n)      35, BYTE(n)
#define STLOC_POP(n)  33, BYTE(n)
#define STLOC(n)      34, BYTE(n)

#define ADD_FUNC(name, arg, rv, loc) {.code=name, .size=sizeof name, .n_args=arg, .n_ret_vals=rv, .n_locals=loc}
#define ADD_FUNC_EX(name, xsize, arg, rv, loc) {.code=name, .n_args=arg, .n_ret_vals=rv, .n_locals=loc, .size = xsize}

struct func_t {
  u32 ptr_to_raw;  // +00
  u32 size_of_raw; // +04
  i8 n_args;     // +08
  u8 n_return_vals; // +09
  i16 n_locals;   // +0A
};

struct code_t {
  u64 magic;       // +00
  u32 ptr_to_r14;      // +08
  u32 size_of_r14;     // +0C
  u32 virtual_size_r14;    // +10
  u32 ptr_to_r13;      // +14
  u32 size_of_r13;     // +18
  u32 virtual_size_r13;    // +1C
  int n_functions;      // +20
};

u8 buffer[0x4000];

struct ifunc_t {
  u8* code;
  size_t size;
  int n_args;
  int n_ret_vals;
  int n_locals;
};

struct jmp_table_t {
  char name[128];
  int index;
} g_JumpTable[0x1000], g_Locals[0x1000], g_Args[0x1000];

void jmp_table_add(char* name) {
  static int index = 0;
  strcpy(g_JumpTable[index].name, name);
  g_JumpTable[index].index = index;
  index++;
}

void argv_add(char* name) {
  static int index = 0;
  strcpy(g_Args[index].name, name);
  g_Args[index].index = index;
  index++;
}

int argv_get(char* name) {
  int i;
  for (i = 0; i < sizeof(g_Args)/sizeof(*g_Args); i++) {
    if (strcmp(name, g_Args[i].name) == 0) {
      return g_Args[i].index;
    }
  }
  return -1;
}

int locals_get(char* name) {
  int i;
  for (i = 0; i < sizeof(g_Locals)/sizeof(*g_Locals); i++) {
    if (strcmp(name, g_Locals[i].name) == 0) {
      return g_Locals[i].index;
    }
  }
  return -1;
}

void locals_add(char* name) {
  static int index = 0;
  strcpy(g_Locals[index].name, name);
  g_Locals[index].index = index;
  index++;
}

int jmp_table_get(char* name) {
  int i;
  for (i = 0; i < sizeof(g_JumpTable)/sizeof(*g_JumpTable); i++) {
    if (strcmp(name, g_JumpTable[i].name) == 0) {
      return g_JumpTable[i].index;
    }
  }
  printf("no jmp index\n");
  exit(43);
}

struct insn_t {
  char const* name;
  int opcode;
  int args_size;
  int n_args;
} g_Instructions[] = {
"PUSH32",        8, 4, 1,
"PUSH16",        9, 2, 1,
"PUSH8",         10, 1, 1,
"DUP",           2, 0, 0,
"POP",           1, 0, 0,
"CALL",          16, 2, 1,
"JMP_END",       17, 0, 0,
"MAKE_LABEL",    18, 0, 0,
"JMP",           19, 2, 1,
"JNZ",           20, 2, 1,
"AND",           80, 0, 0,
"OR",            81, 0, 0,
"XOR",           82, 0, 0,
"ADD",           83, 0, 0,
"SUB",           84, 0, 0,
"SHL",           92, 0, 0,
"SHR",           93, 0, 0,
"SAR",           94, 0, 0,
"IS_EQ_ZERO",    64, 0, 0,
"IS_NE_ZERO",    65, 0, 0,
"CMP_EQ",        66, 0, 0,
"CMP_NE",        67, 0, 0,
"CMP_LT",        68, 0, 0,
"CMP_GT",        69, 0, 0,
"CMP_LE",        70, 0, 0,
"CMP_GE",        71, 0, 0,
"CMP_ULT",       72, 0, 0,
"CMP_UGT",       73, 0, 0,
"CMP_ULE",       74, 0, 0,
"CMP_UGE",       75, 0, 0,
"LDLOC",         32, 1, 1,
"LDARG",         35, 1, 1,
"STLOC_POP",     33, 1, 1,
"STLOC",         34, 1, 1,
// load arb
"LOAD_32_R13",         0x30, 0, 0,   // pop rsi ; mov eax, dword ptr [rsi + r13]; push rax
"SIGN_EXT_LOAD_16_R13",   0x31, 0, 0,   // pop rsi ; movsx eax, word ptr [rsi + r13]; push rax
"SIGN_EXT_LOAD_8_R13",    0x32, 0, 0,   // pop rsi ; movsx eax, byte ptr [rsi + r13]; push rax
"ZERO_EXT_LOAD_16_R13",   0x33, 0, 0,   // pop rsi ; movzx eax, word ptr [rsi + r13]; push rax
"ZERO_EXT_LOAD_8_R13",    0x34, 0, 0,   // pop rsi ; movzx eax, byte ptr [rsi + r13]; push rax
"STORE_32_R13", 0x35, 0, 0,   // pop rdi; pop rax; mov [r13+rdi], eax;
"STORE_16_R13", 0x36, 0, 0,   // pop rdi; pop rax; mov [r13+rdi], ax
"STORE_8_R13",  0x37, 0, 0,   // pop rdi; pop rax; mov [r13+rdi], al
"LOAD_32_R14",         0x38-0x31, 0, 0,   // pop rsi ; mov eax, dword ptr [rsi + r14]; push rax
"SIGN_EXT_LOAD_16_R14",   0x39-0x31, 0, 0,   // pop rsi ; movsx eax, word ptr [rsi + r14]; push rax
"SIGN_EXT_LOAD_8_R14",    0x3A-0x31, 0, 0,   // pop rsi ; movsx eax, byte ptr [rsi + r14]; push rax
"ZERO_EXT_LOAD_16_R14",   0x3B-0x31, 0, 0,   // pop rsi ; movzx eax, word ptr [rsi + r14]; push rax
"ZERO_EXT_LOAD_8_R14",    0x3C-0x31, 0, 0,   // pop rsi ; movzx eax, byte ptr [rsi + r14]; push rax
};

struct insn_t* find_instruction(char* name) {
  int index = 0;
  while (index < sizeof(g_Instructions) / sizeof(g_Instructions[0])) {
    if (strcmp(g_Instructions[index].name, name) == 0) {
      return &g_Instructions[index];
    }
    index++;
  }
  printf("Panik - %s!!\n", name);
  exit(1337);
}

char backup[0x1000];

struct ifunc_t parse_code(char* code) {
  struct ifunc_t ifunc;
  strcpy(backup, code);
  char const* delims = " \n\t\r";
  struct insn_t* mk_tbl = find_instruction("MAKE_LABEL");
  size_t size = 0;
  char* token = strtok(code, delims);
  struct insn_t* t;
  char save[100];
  int n_args = 0, n_locs = 0, n_ret = 0;
  do {
    // printf("token: %s, ", token);
    strcpy(save, token);
    char* p;
    if (p = strstr(save, ":")) {
      *p = 0;
      jmp_table_add(save);
      t = mk_tbl;
    }
    else {
      t = find_instruction(save);
    }
    size += t->args_size + 1;
    if (t->n_args) {
      token = strtok(NULL, delims);
      if (!strcmp(save, "LDLOC") || !strcmp(save, "STLOC") || !strcmp(save, "STLOC_POP")) {
        int pos = locals_get(token);
        if (pos == -1) {
          locals_add(token);
          ++n_locs;
        }
      }
      if (!strcmp(save, "LDARG")) {
        int pos = argv_get(token);
        if (pos == -1) {
          argv_add(token);
          ++n_args;
        }
      }
    }
  } while (token = strtok(NULL, delims));

  printf("[parser] size of code: %zd bytes\n", size);
  u8* buffer = (u8*)calloc(1, size);
  u8* ptr = buffer;
  
  token = strtok(backup, delims);
  do {
    strcpy(save, token);
    // printf("token: %s, ", token);
    if (strstr(save, ":")) {
      t = mk_tbl;
    }
    else {
      t = find_instruction(save);
    }
    *ptr++ = t->opcode;
    printf("%s[%02x] : ", save, t->opcode);
    // size += t->args_size + 1;
    if (t->n_args) {
      token = strtok(NULL, delims);
      if (!strcmp(save, "JMP") || !strcmp(save, "JNZ")) {
        int pos = jmp_table_get(token);
        memcpy(ptr, &pos, 2);
        ptr += 2;
        printf("jmp_to=%d", pos);
      }
      else {
        size_t arg;
        if (!strcmp(save, "LDLOC") || !strcmp(save, "STLOC") || !strcmp(save, "STLOC_POP")) {
          arg = (size_t) locals_get(token);
        }
        else if (!strcmp(save, "LDARG")) {
          arg = (size_t) argv_get(token);
        }
        else {
          sscanf(token, "%zi", &arg);
        }
        switch (t->args_size) {
        case 4:
          printf("u32=%zx", arg);
          memcpy(ptr, &arg, 4);
          ptr += 4;
          break;
        case 2:
          printf("u16=%zx", arg & 0xffff);
          memcpy(ptr, &arg, 2);
          ptr += 2;
          break;
        case 1:
          printf("u8=%zx", arg & 0xff);
          *ptr++ = arg;
          break;
        default:
          printf("Panik2!!!\n");
          exit(34);
        }
      }
    }
    printf("\n");
  } while (token = strtok(NULL, delims));

  ifunc.code = buffer;
  ifunc.n_args = n_args;
  ifunc.n_locals = n_locs;
  ifunc.n_ret_vals = -1;
  ifunc.size = size;

  return ifunc;
}

struct ifunc_t load_file(char* filename) {
  // done by copilot
  FILE* f = fopen(filename, "rb");
  if (!f) {
    printf("Panik - can't open file %s\n", filename);
    exit(1337);
  }
  fseek(f, 0, SEEK_END);
  size_t size = ftell(f);
  fseek(f, 0, SEEK_SET);
  char* code = (char*)malloc(size);
  fread(code, 1, size, f);
  fclose(f);
  return parse_code(code);
}

void dump_hex(u8* buffer, size_t size) {
  size_t i;
  for (i = 0; i < size; i++) {
    printf("%02x ", buffer[i]);
    if (i % 16 == 15) {
      printf("\n");
    }
  }
  printf("\n");
}

int main() {
  struct code_t* code = (struct code_t*)
    buffer;

  struct ifunc_t md = load_file("solve.code");
  printf("[parser]: args:%d, locals: %d\n", md.n_args, md.n_locals);
  md.n_ret_vals = 1;
  printf("Assembler:\n");
  dump_hex(md.code, md.size);

  struct ifunc_t funcs[] = {
    ADD_FUNC_EX(md.code, md.size, md.n_args, md.n_ret_vals, md.n_locals),
  };

  code->magic = 0xD6D733270727F;
  
  code->n_functions = sizeof funcs / sizeof *funcs;

  struct func_t* func_list = (struct func_t*)(((uintptr_t)&code->n_functions) + 4);
  u8* ptr = (u8*)&func_list[code->n_functions];
  for (int i = 0; i < code->n_functions; i++) {
    func_list[i].ptr_to_raw = (uintptr_t)ptr - (uintptr_t)code;
    func_list[i].size_of_raw = funcs[i].size;
    func_list[i].n_args = funcs[i].n_args;
    func_list[i].n_return_vals = funcs[i].n_ret_vals;
    func_list[i].n_locals = funcs[i].n_locals;
    memcpy(ptr, funcs[i].code, funcs[i].size);
    ptr += funcs[i].size;
  }

  code->size_of_r14 = 0x100;
  code->virtual_size_r14 = 0x100;
  code->size_of_r13 = 0x100;
  code->virtual_size_r13 = 0x100;

  char r14[] = "HELLO WORLD OF R14"; // RW
  char r13[] = "HELLO WORLD OF R13"; // R

  memcpy(ptr, r14, sizeof r14);
  memcpy(ptr+code->size_of_r14, r13, sizeof r13);

  code->ptr_to_r14 = ptr - (uintptr_t)code;
  code->ptr_to_r13 = ptr+code->size_of_r14 - (uintptr_t)code;

  uint32_t code_size =
    sizeof(struct code_t) +
    (sizeof(struct func_t)) * code->n_functions +
    code->size_of_r14 +
    code->size_of_r13;

  for (int i = 0; i < code->n_functions; i++) {
    code_size += funcs[i].size;
  }

  printf("code_size: %d\n", code_size);
  FILE* fp = fopen("code.bin", "wb");
  fwrite(code, code_size, 1, fp);
  fclose(fp);

  fp = fopen("to_server.bin", "wb");
  fwrite((u16*)&code_size, 2, 1, fp);
  fwrite(code, code_size, 1, fp);
  fclose(fp);
}
