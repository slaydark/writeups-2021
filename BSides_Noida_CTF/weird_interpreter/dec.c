__int64 __fastcall sub_2750(const char *input)
{
  int v2; // er15
  char src2; // r13
  char src1; // r14
  char v5; // cl
  int pc; // ebp
  char v7; // al
  int v8; // esi
  char dest; // bl
  int v10; // edi
  int i; // edx
  ctx ctx; // [rsp+8h] [rbp-A0h]
  char v14[24]; // [rsp+50h] [rbp-58h] BYREF
  unsigned __int64 v15; // [rsp+68h] [rbp-40h]

  v15 = __readfsqword(0x28u);
  *(_QWORD *)ctx.R = 0LL;
  *(_QWORD *)ctx.buf = 1LL;
  *(_QWORD *)&ctx.buf[4] = 0LL;
  *(_QWORD *)&ctx.buf[8] = 0LL;
  *(_QWORD *)&ctx.buf[0xC] = 0LL;
  *(_QWORD *)ctx.field_28 = 0LL;
  *(_QWORD *)&ctx.field_28[4] = 0LL;
  *(_QWORD *)&ctx.field_28[8] = 0LL;
  *(_DWORD *)&ctx.field_28[0xC] = 0;
  input_size = strlen(input);
  src2 = 0;
  src1 = 0;
  is_running = 1;
  pc = 0;
  while ( is_running )
  {
    opecode = input[pc] | 0x20;
    v8 = pc + 2;
    dest = input[pc + 1] - 0x30;
    if ( opecode == 'c' )
    {
      pc += 2;
    }
    else
    {
      v10 = pc + 3;
      src1 = input[v8] - 0x30;
      if ( opecode != 'r' && opecode != 'w' )
      {
        pc += 4;
        src2 = input[v10] - 0x30;
      }
      else
      {
        pc += 3;
      }
      if ( (unsigned __int8)(input[v8] - 0x30) > 3u && (unsigned __int8)dest > 3u && (unsigned __int8)src2 > 3u )
      {
        std::operator<<<std::char_traits<char>>(&std::cerr, "Invalid Register\n");
        exit(0xFFFFFFFF);
      }
    }
    switch ( opecode )
    {
      case 'a':
        ctx.R[dest] = ctx.R[src1] + ctx.R[src2];
        goto LABEL_6;
      case 'c':
        std::__ostream_insert<char,std::char_traits<char>>(std::cout, "\nChecking...\n", 0xDLL);
        for ( i = 0; i <= 0xF; ++i )
          v14[i] = ctx.buf[i + dest];
        v14[0x10] = 0;
        is_good_str(v14);
        return 1LL;
      case 'd':
        ctx.R[dest] = ctx.R[src1] / ctx.R[src2];
        goto LABEL_6;
      case 'm':
        ctx.R[dest] = ctx.R[src1] * ctx.R[src2];
        goto LABEL_6;
      case 'r':
        ctx.R[dest] = ctx.buf[ctx.R[src1]];
        goto LABEL_6;
      case 's':
        ctx.R[dest] = ctx.R[src1] - ctx.R[src2];
        goto LABEL_6;
      case 'w':
        ctx.buf[ctx.R[dest]] = ctx.R[src1];
LABEL_6:
        if ( pc >= input_size )
          is_running = 0;
        if ( ++n_opcodes > 84 )
        {
          std::__ostream_insert<char,std::char_traits<char>>(&std::cerr, "Too many opcodes\n", 0x11LL);
          is_running = 0;
        }
        break;
      default:
        std::__ostream_insert<char,std::char_traits<char>>(&std::cerr, "Invalid Opcode\n", 0xFLL);
        exit(0xFFFFFFFF);
    }
  }
  return 0LL;
}
