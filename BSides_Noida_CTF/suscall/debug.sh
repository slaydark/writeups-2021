#!/bin/sh
qemu-system-x86_64\
    -m 128M -initrd debugfs.cpio.gz\
    -kernel ./bzImage -nographic -monitor /dev/null\
    -append "kpti=1 kaslr root=/dev/ram rw console=ttyS0 oops=panic paneic=1 quiet"\
    -gdb tcp::12345 \
    2>/dev/null
