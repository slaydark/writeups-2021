from ptrlib import *

src    = [0x5c, 0x49, 0x13, 0x00]
dst    = [0x5c, 0x00, 0x00, 0x00]
answer = [0x2e, 0x2e, 0x00, 0x00]
def execute(payload):
    global src, dst, answer
    for i in range(len(payload) // 2):
        if payload[2*i] == payload[2*i+1]: continue
        # A,B in {0,1,2}
        A, B = ord(payload[2*i])-0x31, ord(payload[2*i+1])-0x31
        x = min(src[B]-dst[B], dst[A])
        print(hex(x), hex(src[B]-dst[B]), hex(dst[A]))
        dst[A] -= x
        dst[B] += x

mask_on = b"true"

payload  = "12233123312331231223312331233123312312233123312331233123122331233123312331231223312331233123312312233123312331233123122331233123312312233123312331233123122331233123312331231223312331"
payload += '1' * (0xb6 - len(payload))
execute(payload)
# assert dst == answer

sock = Process(["./0xC0F1D"], env={"MASK_ON": mask_on})

sock.sendlineafter(": ", payload)

sock.interactive()
