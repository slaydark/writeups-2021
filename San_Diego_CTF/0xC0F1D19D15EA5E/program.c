char md5sum_answer[] = {
  0xB3, 0x26, 0xB5, 0x06, 0x2B, 0x2F, 0x0E, 0x69, 0x04, 0x68,
  0x10, 0x71, 0x75, 0x34, 0xcB, 0x09
};
int foo[];
unsigned char enc[] = {0x14, 0x97, 0xfa, 0xf1, 0x3b, 0x3b, 0x97, 0xca, 0xd7, 0xd3, 0xed, 0xca, 0x97, 0xd3, 0xd7, 0x70, 0xd3, 0x14, 0xf1, 0x89, 0xfa};
unsigned char ponta[] = {0x74, 0x42, 0x51, 0x59, 0x87, 0x53, 0xd9, 0xc0, 0x92, 0xe8, 0x5e, 0x6a, 0xa4, 0xdc, 0x59, 0x6b, 0x5e, 0xf3, 0x2b, 0x29, 0x73, 0xeb, 0x15, 0x63, 0x38, 0x56, 0xe8, 0x50, 0xa4, 0x81, 0xd0, 0xda};

void validate() {
  unsigned char password[0xc0];
  printf("Enter password: ");
  readline(password, 0xb8);
  if (strlen(password) != 0xb6) abort();
  for (int i = 0; i < 0xb6; i++) {
    if (password[i] <= 0x30 || password[i] > 0x33) abort();
  }
  for (int i = 0; i < 0x5a; i++) {
    A = password[i*2] - 0x31;
    B = password[i*2+1] - 0x31;
    // signal jump here
    if (A != B) {
      int x;
      if (foo[B] - foo[3+B] >= foo[3+A]) {
        x = foo[3+A];
      } else {
        x = foo[B] - foo[3+B];
      }
      foo[3+A] -= x;
      foo[3+B] += x;
    }
  }
  for (int i = 0; i < 3; i++) {
    if (foo[3+i] != foo[4+i]) abort();
  }
}

void dummy_validate() {
  unsigned char password[0x20];
  printf("Enter password: ");
  readline(password, 0x17);
  if (strlen(password) != 21) abort();
  for (int i = 0; i < 21; i++) {
    if (password[i] * 13 != enc[i])
      abort();
  }
  sha256pass = SHA256(password, 21, 0);
  for (int i = 0; i < 32; i++) {
    ponta[i] ^= sha256pass[i];
  }
}

int main() {
  int result = 0;
  char *mask_on = getenv("MASK_ON");
  if (mask_on == NULL) goto ERR;
  if (strlen(mask_on) == 4) {
    if (CRYPTO_memcmp(MD5(mask_on, 4, 0), md5sum_answer, 0x10) == 0) {
      result = 1; // reach here
    }
  }
  if ((result & 1) ^ 1) goto ERR;

  char *sha256sum = SHA256(mask_on, 4, 0);
  // be careful of signed extension
  foo[0] = sha256sum[0] ^ 0xE9;
  foo[1] = sha256sum[1] ^ 0xF7;
  foo[2] = sha256sum[2] ^ 0xB7;
  foo[3] = sha256sum[3] ^ 0x47;
  foo[4] = sha256sum[4] ^ 0x42;
  foo[5] = sha256sum[5] ^ 0x4C;
  foo[6] = sha256sum[6] ^ 0x3F;
  validate();

 ERR:
  puts("The computer must be wearing a mask to run this program.");
  return 1;
}
