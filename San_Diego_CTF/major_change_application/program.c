unsigned long time_div4;
char first_name[100];
char last_name[100];

int main() {
  get_time();
  // welcome_banner();
  ask_fullname();
  // do_nothing();
  ask_studentID();
  submit_application();
  // ...
  final();
  return 0;
}

unsigned long gen_hash() {
  unsigned long hash = 0xC0DE5BAD13375EED;
  for (int i = 0; i < strlen(first_name); i++)
    hash = hash * 31 + first_name[i];
  for (int i = 0; i < strlen(last_name); i++)
    hash = hash * 31 + last_name[i];
  return hash;
}

unsigned int randgen(unsigned int seed) {
  srand(seed);
  return rand();
}

unsigned long randgen64(unsigned long seed) {
  return randgen(seed >> 32) + (randgen(seed & 0xffffffff) << 32);
}

int do_lottery() {
  unsigned long x = randgen64(time_div4) - randgen64(gen_hash());
  // ... blah blah
  return x <= 9;
}

void final() {
  if (do_lottery()) {
    puts("FLAG: flag{here}");
  } else {
    puts("sorry text");
  }
}

int strncmp(const *s1, const *s2, int n) {
  int r = 0;
  for (int i = 0; i < n; i++) {
    r |= s1[i] ^ s2[i];
  }
  return r == 0;
}

void submit_application() {
  char ans[6];
  memset(ans, 0, 4);
  puts("\nAre you ready to submit your application?");
  readline(ans, 5);

  for (int i = 0; i < 4; i++) {
    ans[i] = (ans[i] ^ (text[i] - i*i)) + i*i*i;
  }
  if (!strncmp(ans, "6)bc", 4)) {
    puts("Not getting an affirmative response");
    abort_InvalidResponse();
  }
}

void initialize_random() {
  long l1 = strlen(first_name);
  long l2 = strlen(last_name);
  unsigned long seed 0;
  for (long i = 0; i < l1; i++) {
    seed = first_name[i] + 31 * seed;
  }
  for (long i = 0; i < l2; i++) {
    seed = last_name[i] + 31 * seed;
  }
  srand(seed);
}

void ask_studentID() {
  char id[10];
  printf("2. Student ID: ");
  for (int i = 0; i <= 8; i++) {
    char c = getchar();
    id[i] = c;
    if (id[i] == '\n') abort_InvalidID();
  }
  if (getchar() != '\n') abort_InvalidID();
  id[9] = '\0';
  if (id[0] != sub_13e9()) abort_InvalidID(); // 'H'
  char *p;
  long sid = strtol(&id[1], p, 10);
  if (p != &sid[9]) abort_InvalidStudentID(); // assert valid numbers
  if (sid <= 0) abort_InvalidStudentID(); // positive value
  initialize_random();
  if (sid != rand() % 100000000) abort_InvalidStudentID();
}

void ask_fullname() {
  char buf[0x100];
  printf("1. Name (Last, First): ");
  readline(buf, 0xfa);

  // Check format: "LAST,FIRST" or "LAST FIRST"
  char *last = strtok(buf, ", ");
  char *first = strtok(NULL, ", ");
  if (first == NULL || last == NULL || strrok(NULL, ", "))
    abort_InvalidName();
  if (strlen(first) > 0x63 || strlen(last) > 0x63)
    abort_InvalidName();

  // Copy
  strncpy(first_name, first, 100);
  strncpy(last_name, last, 100);
}

void get_time() {
  time_div4 = time(NULL) >> 2;
}
