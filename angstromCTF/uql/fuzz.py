import random
from ptrlib import *

def insert(data):
    return f' insert {data}'
def remove(data):
    return f' remove {data}'
def modify(data, index, char):
    return f' modify {data} to be {char} at {index}'
def display():
    return " display everything"
def execute(code):
    sock.sendlineafter("> ", code)

sock = Socket("localhost", 9999)

while True:
    code = ''
    for i in range(random.randint(1, 10)):
        choice = random.randint(0, 3)
        if choice == 0:
            code += display()
        elif choice == 1:
            v = 'A' * random.randrange(1, 2)
            code += insert(v)
        elif choice == 2:
            v = 'A' * random.randrange(1, 2)
            code += remove(v)
        elif choice == 3:
            n = random.randrange(1, 2)
            v = 'A' * n
            i = random.randrange(0, n)
            code += modify(v, i, 'X')
    execute(code)
    print(code)

sock.interactive()
