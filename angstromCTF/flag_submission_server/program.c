// sizeof(State) == 0x38
typedef struct {
  char buffer[0x16]; // stack0xffffffffffc7fe88
  char uid[13];      // stack0xffffffffffc7feac
  int cnt;           // stack0xffffffffffc7fea0
  int Y;             // stack0xffffffffffc7fea4
  int offset;        // stack0xffffffffffc7fea8
} State;

unsigned long gen_rand(unsigned int seed)
{
  srand(seed ^ 12345678);
  unsigned int r;
  for (int i = 0; i < 8; i++) {
    r = r*10 + rand()%10;
  }
  return (unsigned long)r * (unsigned long)seed;
}

int main() {
  State state[16];
  char secret[] = "\xde\xad\xbe\xef\xfe\xed\xca\xfe\x13\x37\xab\xcd\xef";
  fd_set readfds[16], expectfds[16], fdList[16];
  char flag[0x40];
  // ... socket setup here ...
  int server_fd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
  // ... socket setup here ...
  bind(server_fd, res->ai_addr, res->ai_addrlen);
  listen(server_fd, 16);
  memset(?, 0, 0x10 * sizeof(unsigned long));
  // ???
  FILE *fp_urandom = fopen("/dev/urandom", "r");
  FILE *fp_flag = fopen("flag.txt", "r");
  fgets(flag, 0x40, fp_flag);

  while (1) {
    memcpy(readfds, fdList, sizeof(fdList));
    memcpy(expectfds, fdList, sizeof(fdList));
    select(server_fd + 1, readfds, NULL, expectedfds, 0);
    for (int fd = 0; fd < server_fd + 1; fd++) {
      // some check on fd here (loc_10ec, loc_1189)
      /* Here is the main process */

      if (fd == server_fd) {
        /* New connection request */

        int client_fd = accept(server_fd, 0, 0);
        // Update fdList here (loc_1217)
        for (int i = 0; i < 13; i++) {
          char c = fgetc(fp_urandom);
          state[client_fd]->uid[i] = c;
          c ^= secret[i];
          send(client_fd, &c, 1, 0);
        }
        state[client_fd]->cnt = 0;
        state[client_fd]->Y = 0x13371337;
        state[client_fd]->Z = 0;

      } else {
        /* New message from client */

        char buffer[0x100];
        int length = recv(fd, buffer, 0x100, 0);
        if (length == 0) {
          // not important maybe
          continue;
        }

        // loc_1996
        for (int i = 0; i < length; i++) {
          state[client_fd]->cnt++;
          state[client_fd]->buffer[i] = buffer[i];
          if (state[client_fd]->cnt == 22) {
            // check ID
            int is_okay = 1;
            for (int k = 0; k < 13; j++) {
              // loc_15c4
              if (state[client_fd]->buffer[9+k] != state[client_fd]->uid[k])
                is_okay = 0;
            }
            unsigned long Y = (*unsigned long)state[client_fd]->buffer;
            if (gen_rand(state[client_fd]->Y) != Y) {
              is_okay = 0;
            }
            if (state[client_fd]->buffer[8] == flag[state[client_fd]->offset]) {
              if (is_okay) {
                state[client_fd]->offset++;
                state[client_fd]->cnt = 0;
                state[client_fd]->Y = 0;
                send(client_fd, "%", 1, 0);
                for (int k = 0; k < 4; k++) {
                  char c = rand() % 0x100;
                  send(client_fd, &c, 1, 0);
                  state[client_fd]->Y = (state[client_fd]->Y << 8) | c;
                }
              } else {
                // close connection
              }
            } else {
              send(client_fd, "\x0d", 1, 0);
            }
          }
        }
      }

    }
  }
  return 0;
}
