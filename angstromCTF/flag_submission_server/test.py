from ptrlib import *

sock = Socket("localhost", 21450)
#sock = Socket("rev.2021.chall.actf.co", 21450)

# get uuid
key = b"\xde\xad\xbe\xef\xfe\xed\xca\xfe\x13\x37\xab\xcd\xef"
s = b''
while len(s) != 13:
    s += sock.recv()
uuid = xor(s, key)
logger.info("UUID = " + uuid.hex())

payload  = p64(0xdeadbeefcafebabe)
payload += b'a'
payload += b'X' * 13
sock.send(payload)
print(sock.recv())

sock.interactive()

