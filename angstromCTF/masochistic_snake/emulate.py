import ctypes

def reset(field):
    while True:
        offset = libc.rand() % 0x100
        if field[offset] == 0:
            field[offset] = 2
            break
    for i in range(50):
        while True:
            offset = libc.rand() % 0x100
            if field[offset] == 0:
                field[offset] = 3
                break

def show(field):
    output = ''
    for y in range(16):
        for x in range(16):
            output += ['.', '*', '$', 'x'][field[y*16+x]]
        output += '\n'
    print(output)

libc = ctypes.CDLL('/lib/x86_64-linux-gnu/libc-2.27.so')

field = [0 for i in range(0x100)]
field[0] = 1
reset(field)
show(field)
