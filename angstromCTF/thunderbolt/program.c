unsigned int f[0x100];
unsigned int s = 0;

void fgen(unsigned char *fbuf, unsigned char *key, int len_in, int len_key)
{
  for (int i = 0; i < 0x100; i++) {
    f[i] = i;
  }
  for (int i = 0; i < 0x300; i++) {
    s = f[(fbuf[i%len_in] + f[i%0x100] + s) % 0x100];
    swap(&f[i%0x100], &f[s]);
  }
  for (int i = 0; i < 0x300; i++) {
    s = f[(key[i%len_key] + f[i%0x100] + s) % 0x100];
    swap(&f[i%0x100], &f[s]);
  }
}

void enc(unsigned char *plaintext, unsigned char *cipher, int len_in)
{
  int j;
  for (int i = 0; i != len_in; i++) {
    s = f[(s + f[i%0x100]) % 0x100];
    cipher[i] = plaintext[i] ^ f[(f[f[s]] + 1) % 0x100];
    swap(&f[i%0x100], &f[s]);
  }
}

int main()
{
  FILE *fp;
  int len_plaintext, len_flag, n = 0;
  unsigned char *fbuf, *cipher, *plaintext = NULL;
  unsigned char flag[0x100], key[0x10];

  /* Read plaintext */
  setvbuf(stdout, NULL, _IONBF, 0);
  printf("Enter a string to encrypt: ");
  getline(&plaintext, &n);
  len_plaintext = strcspn(plaintext, "\n");

  /* Read flag */
  if (!(fp = fopen("flag", "rb"))) {
    puts("Could not open flag file.");
    return 1;
  }
  fgets(flag, 0x100, fp);
  len_flag = strcspn(flag, "\n");
  flag[len_flag] = 0;

  /* Concat plaintext + flag (<plaintext><flag>\0) */
  plaintext = (unsigned char*)realloc(plaintext, len_plaintext + len_flag + 1);
  strcpy(&plaintext[len_plaintext], flag);

  /* Generate something */
  fp = fopen("/dev/urandom", "rb");
  fread(key, 0x10, fp); // key = random 16 bytes
  fbuf = (unsigned char*)malloc(len_plaintext + len_flag);
  fread(fbuf, 1, fp);
  fgen(fbuf, key, len_plaintext + len_flag, 0x10);

  /* Encrypt */
  cipher = (unsigned char*)malloc(len_plaintext + len_flag);
  enc(plaintext, cipher, len_plaintext + len_flag);

  /* Show result */
  for (int i = 0; i < len_plaintext + len_flag; i++) {
    printf("%02x", cipher[i]);
  }
  putchar('\n');
  free(cipher);
  free(plaintext);
  free(fbuf);
}
