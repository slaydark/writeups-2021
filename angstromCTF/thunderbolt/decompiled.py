import os

s = 0
f = [i for i in range(0x100)]
def fgen(rndbuf, key, len_in):
    global f, s
    for i in range(0x300):
        s = f[(rndbuf[i%len_in] + f[i%0x100] + s) % 0x100] % 0x100
        if i%0x100 == s:
            f[i%0x100] = f[s] = 0 # vulnerability!
        else:
            f[i%0x100], f[s] = f[s], f[i%0x100]
    print(f)
    exit()
    for i in range(0x300):
        s = f[(key[i%len(key)] + f[i%0x100] + s) % 0x100] % 0x100
        if i%0x100 == s:
            f[i%0x100] = f[s] = 0
        else:
            f[i%0x100], f[s] = f[s], f[i%0x100]
def enc(plaintext):
    global f, s
    cipher = b''
    for i in range(len(plaintext)):
        s = f[(s + f[i%0x100]) % 0x100]
        cipher += bytes([plaintext[i] ^ f[(f[f[s]] + 1) % 0x100]])
        if i%0x100 == s:
            f[i%0x100] = f[s] = 0
        else:
            f[i%0x100], f[s] = f[s], f[i%0x100]
    return cipher

#plaintext = input("Plaintext: ").encode()
plaintext = b'AAAA'
with open("flag", "rb") as fp:
    flag = fp.read().rstrip()
plaintext += flag

"""
key = os.urandom(0x10)
rndbuf = os.urandom(len(plaintext))
with open("key.bin", "wb") as fp:
    fp.write(key)
with open("rndbuf.bin", "wb") as fp:
    fp.write(rndbuf)
"""
with open("key.bin", "rb") as fp:
    key = fp.read()
with open("rndbuf.bin", "rb") as fp:
    rndbuf = fp.read()

fgen(rndbuf, key, len(plaintext))
print(f)
cipher = enc(plaintext)
print(cipher.hex())
