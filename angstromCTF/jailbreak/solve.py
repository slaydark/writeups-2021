from ptrlib import *

#sock = Process("./jailbreak")
sock = Socket("nc shell.actf.co 21701")

sock.sendlineafter("?\n", "pick the snake up")
sock.sendlineafter("?\n", "throw the snake at kmh")
sock.sendlineafter("?\n", "pry the bars open")

n = 1337
r12 = 1
for i in range(n.bit_length()-2, -1, -1):
    if n >> i == r12 * 2:
        sock.sendlineafter("?\n", "press the red button")
        r12 += r12
    else:
        sock.sendlineafter("?\n", "press the green button")
        r12 += r12 + 1

sock.sendlineafter("?\n", "bananarama")

sock.interactive()
