int ofsList[] = {
  0, 0x1a, 0x7b, ...., 0x5e5, 0x6ef
}

char *gen_string(int id) {
  int size = ofsList[id+1] - ofsList[id];
  char *data = malloc(size+1);
  data[size] = 0;
  int key = id;
  for (int i = 0; i < size; i++) {
    data[i] = key ^ globalData[i];
    key = 16*id + data[i]*key;
  }
  return data;
}

void show_message(int id) {
  char *s = gen_message();
  puts(s);
  free(s);
}
